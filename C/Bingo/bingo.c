/*
Autor:Jiale Cheng
Programa:Bingo.c
Descripci�n: Pr�ctica de primero. Juego de bingo autom�tico y aleatorio con posibilidad de guardado
            y carga de partida.
*/
#include<stdio.h>
#include<stdlib.h>

struct elemento {//structura de los cartones
   int carton;
   int posicion;
   int valor;
   int jugada;
};

struct elemento c1[5][5];//jugador
struct elemento c2[5][5];//m�quina

int aleatorio(){ //funci�n que genera un n�mero aleatorio
   int num;
   num=rand()% 79+1;
   return num;
}

int existenumero(struct elemento c[5][5], int n){ //comprueba si el n�mero
                                                 //generado existe
   int i, j;
   for(i=0;i<5;i++){
      for(j=0;j<5;j++){
         if(c[i][j].valor==n){
            return 1;                    
         }  
      }
   }
    return 0;
}

void inicializacarton(struct elemento c[5][5], int num){//crea los cartones y
   int i, j;                                            //los rellena de n�meros
   int posicion = 0;
   int valor = 0;
   for(i=0; i<5; i++){
      for(j=0;j<5;j++){
         posicion++;
         c[i][j].carton=num;
         valor=aleatorio();
         while(existenumero(c,valor)!=0){
            valor=aleatorio();                        
         } 
         c[i][j].posicion=posicion; 
         c[i][j].valor=valor;
         c[i][j].jugada=0;          
      }
   }
}

void muestracarton(struct elemento c[5][5], int num){//muestra los cartones
   int i, j;
   for(i=0; i<5; i++){
      for(j=0; j<5; j++){
         if(c[i][j].jugada==0){
            printf("%i\t",c[i][j].valor);
         }
         else{//si el n�mero ya ha salido �ste ser� m�rcado
            printf("<%i>\t",c[i][j].valor);
         }
      }
   printf("\n");
   } 
}

int compruebalinea(struct elemento c[5][5]){//comprueba si se hace l�nea
   int i,j,contador;                        //en horizontal  
   for(i=0;i<5;i++){       
      contador=0;
      for(j=0;j<5;j++){  
         if(c[i][j].jugada!=0){
            contador++;
         }
         if(contador==5){
            return 1;            
         }
      }
   }
 /*  for(j=0;j<5;j++){       
      contador=0;
      for(i=0;i<5;i++){  
         if(c[i][j].jugada!=0){
            contador++;
         }
         if(contador==5){
            return 1;
         } 
      }  
   }*/ //se canta linea s�lo en horizontal
    return 0;
}
int compruebabingo(struct elemento c[5][5]){//comprueba si se ha ganado
   int i, j;
   int cont_a;
   int cont_b=0;
   for(i=0; i<5; i++){
      cont_a=0;
      for(j=0; j<5; j++){    //se parece a compruebalinea pero en las 5 filas
         if(c[i][j].jugada==1){
            cont_a++;
         }
         if(cont_a==5){
            cont_b++;
         }
      }
      if(cont_b==5){
         return 1;
      }
   }
   return 0;
}
void guardapartida(struct elemento c1[5][5],struct elemento c2[5][5]){//guarda
   printf("\nIndique el nombre del archivo de guardado: ");
   char name[80];
   gets (name);
   fflush(stdin);
   int i, j;
   FILE *fichero;
   fichero=fopen(name,"wb");//abrimos el fichero
   if(!fichero ){ //en caso de cualquier problema y no se abra
      printf( "ERROR\n" ); 
   }
   else{
      printf( "ABIERTO\n" );
   }
   for(i=0;i<5;i++){       
      for(j=0;j<5;j++){ 
         fwrite(&c1[i][j], sizeof(struct elemento), 1, fichero);
      }
   }
   for(i=0;i<5;i++){       
      for(j=0;j<5;j++){ 
         fwrite(&c2[i][j], sizeof(struct elemento), 1, fichero);
      }
   } //ponemos la estructura en los ficheros
   if (!fclose(fichero)){
      printf("CERRADO\n");
   }
   else{
      printf("ERROR\n");
   }            
}
void cargapartida(struct elemento c1[5][5],struct elemento c2[5][5]){//carga
   FILE *fichero;
   char nombre[100];
   int i=1;
   int j;
   while(i=1){//esto se repetir� hasta encontrar una partida existente
      printf("Introduzca la partida que quiere cargar: ");
      gets(nombre);
      fflush(stdin);
      fichero=fopen(nombre, "r");
      if(fichero == NULL){
         printf("\n Esa partida no existe\n");
      }
      else{
         i=2;
         break;
      }
   }
   while(!feof(fichero)){//feof nos permite saber si a�n quedan caracteres por leer
                         //(End Of File)
      for(i=0;i<5;i++){       
         for(j=0;j<5;j++){
            fread(&c1[i][j], sizeof(struct elemento),1 ,fichero);
         }
      }
      for(i=0;i<5;i++){       
         for(j=0;j<5;j++){
            fread(&c2[i][j], sizeof(struct elemento),1 ,fichero);
         }
      }    
   }
   fclose(fichero);
}
int comprobarbola(int bola[80], int b, int j){//comprueba si la bola ya sali�
   int i;
   for(i=0;i<j;i++){
      if(bola[i]==b){
         return 1;
      }
   }
   return 0;
}
int numbola(int bola[80],int j){//generamos una bola aleatoria
   int i;
   i=aleatorio();
   while(comprobarbola(bola, i, j)!=0){//si la bola ya existe volver� ha generar 
      i=aleatorio();//el n�mero aleatorio
   }
   bola[j]=i;
   j++;
   return i;
}
void comprobar(struct elemento c[5][5], int bola){//comprueba la bola con el cart�n
   int i,j;
   for(i=0;i<5;i++){       
      for(j=0;j<5;j++){  
         if(c[i][j].valor==bola){
            c[i][j].jugada=1;
         }
      }
   }   
}

int main(){
   int i;
   int j=0;
   int bola[80];
   int x;
   int linea=0;
   printf("1-Nueva partida\n2-Cargar partida\n3-salir\n");//este ser� el men�
   scanf("%i", &i);
   fflush(stdin);
   system("cls");
   if(i==1){//opci�n 1
      int a;
      srand(time(NULL));
      inicializacarton(c1, 1);//repartimos n�meros del cart�n
      inicializacarton(c2, 2);
      printf("Carton del jugador\n");
      muestracarton(c1, 1);
      printf("Carton de la maquina\n");
      muestracarton(c2,2);
      system("PAUSE");
      while(compruebabingo(c1)!=1 && compruebabingo(c2)!=1){//hasta que no haya bingo
         x=numbola(bola, j);
         comprobar(c1, x);
         comprobar(c2, x);
         if (linea==1){
            printf("\nJugador ha cantado linea\n");
         }
         if (linea==2){
            printf("\nMaquina ha cantado linea\n");
         }
         if (linea==0){
            if(compruebalinea(c1)==1){//despu�s de cada comprobaci�n con el n�mero 
               linea=1;//y el cart�n comprueba si se ha hecho linea
               printf("\nJugador canta linea\n");
            }
            if(compruebalinea(c2)==1){
               linea=2;
               printf("\nMaquina canta linea\n");
            }
         }
         printf("\nbola %i\n", x);//indica de qui�n es el cart�n y los muestra
         printf("\nCarton del jugador\n");
         muestracarton(c1, 1);
         printf("\nCarton de la maquina\n");
         muestracarton(c2,2);   
      }
      if(compruebabingo(c1)==1){
         printf("GANADOR-> JUGADOR\n");
      }
      else{
         printf("GANADOR-> MAQUINA\n");
      }
      printf("\n1-guardar partida\n2-salir\n\n");
      scanf("%i", &a);
      fflush(stdin);
      if (a==1){
         guardapartida(c1, c2);
      }
   }
   if(i==2){//opci�n 2
      cargapartida(c1,c2);//carga la partida
      printf("\nCARTON DEL JUGADOR\n");
      muestracarton(c1, 1);
      printf("\nCARTON DE LA MAQUINA\n");
      muestracarton(c2, 2);
      if (linea==1){
         printf("\nJugador ha cantado linea\n");
      }
      if (linea==2){
         printf("\nMaquina ha cantado linea\n");
      }
      if(compruebabingo(c1)==1){//indica el ganador
         printf("GANADOR-> JUGADOR\n");
      }
      else{
         printf("GANADOR-> MAQUINA\n");
      }
   }
   system("PAUSE");
}




