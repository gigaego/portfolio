/*
   Autor:Jiale Cheng
   Descripcion:practica de hashing.
   programa:hashing_cerrado.c
*/

#include<stdio.h>
#include<string.h>
#define TAM_TABLA 27

typedef struct termino{
      char palabra[25];
}tablahash [TAM_TABLA];

int convierteclave(char clave [25]){
   int resultado=0;
   int i;
   for(i=0; i<strlen(clave); i++){
      resultado= resultado + (int)clave[i];  
   } 
   return resultado;
}

int hash(int clavenum){
   int posicion;
   posicion = clavenum % TAM_TABLA;
   return posicion;
}
void insertar (tablahash tabla[], char clave[25]){
   int i;
   i=hash(convierteclave(clave));
   while(i< TAM_TABLA){
      if(strcmp(tabla[i]->palabra,"")==0){
         strcpy(tabla[i]->palabra, clave);
         break;         
      }
      if(strcmp(tabla[i]->palabra,clave)==0){
         printf("Esa palabra ya fue introducida");
         break;
      }    
      else{
         if(i==TAM_TABLA-1){
            i=0;
         }
         else{
            i++;
         }
      }     
      
   }

}
void vertabla(tablahash tabla[]){
   int i=0;
   while(i<TAM_TABLA){
      if(hash(convierteclave(tabla[i]->palabra))==0){
         i++;
      }
      else{
         printf("%i  ", i);
         puts(tabla[i]->palabra);
         i++;
      }
   }
}

void buscartabla(tablahash tabla[], char clave[25]){
   int i=hash(convierteclave(clave));
   if(strcmp(tabla[i]->palabra,"")==0){
      printf("Esa palabra no se encuentra en la lista\n");
   }
   else{
      while(strcmp(tabla[i]->palabra, "")!=0){
         if(strcmp(tabla[i]->palabra, clave)!=0){
            i++;                             
         }
         else{
            printf("\nSe encuentra en la posicion %i\n", i);
            break;
         }                             
      }
      if(strcmp(tabla[i]->palabra,"")==0){
         printf("Esa palabra no se encuentra en la lista\n");
      }
   }
      
}

void inicializa(tablahash t[]){
   int i;
   for(i=0; i<TAM_TABLA; i++){
      strcpy(t[i]->palabra,"");     
   }
}

int main(){
   tablahash t[TAM_TABLA];
   char termino[25];
   char busqueda[25];
   inicializa(t);
   int i=0;
   printf("TABLA DE 5 ELEMENTOS\n\n");
   while(i!=5){
      printf("Introduzca una palabra:");
      gets(termino);
      insertar( t, termino);
      i++;
   } 
   vertabla(t);
   printf("\nIntroduzca la palabra que quiere buscar: ");
   gets(busqueda);
   buscartabla(t, busqueda);
         
  
system("PAUSE");
}
