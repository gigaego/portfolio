/*
Autor:Jiale Cheng
programa:practica2b.c
descripci�n:simula el juego de la primitiva (pr�ctica de 1�)
*/

#include<stdio.h>
#include "funciones.h"


int main(){
   int apuesta[6], numero[6];//uno es el conjunto de numeros que introducimos y otro son los aleatorios
   int a;//se trata de un contador de los aciertos
   int intentos=1;//es el numero de intentos
   srand(time(NULL));//semillas de numeros aleatorios
   printf("Apuesta: ");
   generar(apuesta);//generamos el numero de la apuesta
   mostrar(apuesta);//mostramos la apuesta
   printf("numero de aciertos que quieres: ");
   scanf("%i", &a);//leemos el numero de aciertos que queremos
   fflush(stdin);
   generar(numero);//generamos el numero que hay que acertar
   
   while(aciertos(apuesta, numero) != a){//mientras sean distintas
      generar(numero);
      printf("\nNumero %i: ", intentos);
      mostrar(numero);   
      intentos++;
      
   }    
   intentos--;
   printf("\nIntento Ganador %i: ", intentos);
   mostrar(numero);
   printf("numero de aciertos: %i", a);
fflush(stdin);
getchar();    
}
