/*
Autor:Jiale Cheng
programa:practica2a.c
descripci�n:simula el juego de la primitiva. (pr�ctica de 1�)
*/

#include<stdio.h>


int comprueba(int m[6], int valor){
   //Busca valor en la matriz y devuelve
   //1 si lo encuentra, 0 en caso contrario
   
   int i;
   for(i=0;i<6;i++){
       if(m[i]==valor)
          return 1;
   }
   return 0;
}


void generar(int m[6]){
     //Esta funci�n almacena 6 elementos aleatorios en la matriz m    
     int generados=0; //numeros ya generados 
     int num;//para cada numero aleatorio que se genere
     
     while(generados<6){
        num=rand()%49+1;
        if(comprueba(m,num)==0){//No existe
           m[generados]=num;
           generados++;
        }   
     }    
}

void mostrar (int m[6]){
   int i;
   for(i=0; i<6; i++){
      printf("%i\t", m[i]);
      //recorremos la matriz mostrando cada valor
   }
   printf("\n");
}       
int aciertos(int m[6], int n[6]){
   int i, l;//se usaran para recorrer los numeros
   int j=0;
   for(l=0;l<6;l++){
      for(i=0;i<6;i++){
         if(m[l]==n[i]){
            j++;//numero de aciertos
         } 
      }
      i=0;
   }
   return j;
}


int main(){
   int apuesta[6], numero[6];//uno es el conjunto de numeros que introducimos y otro son los aleatorios
   int i;
   int j=0;
   srand(time(NULL));//semillas de numeros aleatorios
   generar(numero);//generamos el numero
   generar(apuesta);
   printf("Apuesta: ");
   mostrar(apuesta);
   printf("Ganador: ");
   mostrar(numero);
   printf("\nNumero de aciertos: %i", aciertos(apuesta,numero));
   
fflush(stdin);
getchar();    
}
