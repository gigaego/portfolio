﻿/*
 Creator: Jiale Cheng
 Description: Learning the basics of Unity. Bullet component.
*/


using UnityEngine;
using System.Collections;


/// <summary>
/// Manages the destruction of this gameObject, and the interactions
/// with other entites as this collides with them
/// </summary>
public class Bullet : MonoBehaviour
{
	
	public float timeToDestroyBullet = 2.0f;

	// Use this for initialization
	void Start ()
	{
		Destroy (gameObject, timeToDestroyBullet);
	}
	
	
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag.Equals("Walls"))
		{
			Destroy(gameObject);
		}
    }
	
}
