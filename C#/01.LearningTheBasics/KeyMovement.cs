﻿/*
 Creator: Jiale Cheng
 Description: Learning the basics of Unity. Testing some keyboard movements
*/


using UnityEngine;
using System.Collections;



/// <summary>
/// Moves this GameObject when the keys are pressed
/// </summary>
public class KeyMovement : MonoBehaviour
{
	
	public float speed = 3.0f;
	public float angularSpeed = 3.0f;
	
	public float jumpFactor = 4.0f;
	
	
	public float distanceToGround = 1.0f;
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		//transform.position += Vector3.one * speed * Time.deltaTime;
		
		
		if (Input.GetKey(KeyCode.UpArrow))
		{
			transform.position += transform.forward * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.DownArrow))
		{
			transform.position -= transform.forward * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.LeftArrow))
		{	
			transform.Rotate(new Vector3(0, -angularSpeed * Time.deltaTime, 0));
		}
		if (Input.GetKey(KeyCode.RightArrow))
		{
			transform.Rotate(new Vector3(0, angularSpeed * Time.deltaTime, 0));
		}
		
		
		if (Input.GetKeyDown(KeyCode.Space))
		{
			TryToJump();
		}
		
		
		/*if (Input.GetMouseButtonDown(0))
		{
			SelectPoint(Input.mousePosition);
		}*/
		
		
	}
	
	/// <summary>
	/// Checks that the gruond is below and, if so, performs a jump.
	/// </summary>
	public void TryToJump ()
	{
		
		Debug.DrawRay(transform.position, Vector3.down * distanceToGround, Color.red, 1.0f);
		
		RaycastHit hit;
		if (Physics.Raycast(transform.position, Vector3.down, distanceToGround))
		{
			Debug.Log("Puedo saltar!");
			
			// Make the rigidbody jump
			rigidbody.AddForce(Vector3.up * jumpFactor, ForceMode.Impulse);
		}
	}
	

	
	
}
