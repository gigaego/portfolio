﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.Xml.Serialization;


public class GameManager : MonoBehaviour {

    private float startTime;
    public Text timer;

    private string path = "Application.dataPath";

    private float score = 99999;
    public Text scoreText;

    public List<ColorReceptor> receptors;
    public List<Dice> dices;
    private int sideColor = 0;

    void Start()
    {
        foreach (Dice dice in dices)
        {
            dice.Initialized();
        }
        
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckWin() == true)
        {
            Save();
        }
        float TimerControl = Time.time - startTime;
        string mins = ((int)TimerControl / 60).ToString("00");
        string segs = (TimerControl % 60).ToString("00");
        string milisegs = ((TimerControl * 100) % 100).ToString("00");

        string TimerString = string.Format("{00}:{01}:{02}", mins, segs, milisegs);
        if (CheckWin() != true)
        { 
        timer.text = TimerString.ToString();

        score = score - 2 * Time.deltaTime;
        scoreText.text = score.ToString("F0");
        }
    }

    public bool CheckWin()
    {
        for (int i = 0; i < receptors.Count; i++)
        {
            if (i == 0)
            {
                sideColor = receptors[0].getSide();
            }
            else
            {
                if (receptors[i].getSide() != sideColor)
                    return false;
            }
        }
        return true;
    }

    void Save()
    {
        XmlDocument xmlSav = new XmlDocument();
        XmlElement root = xmlSav.CreateElement("", "root", "");
        xmlSav.AppendChild(root);

        XmlElement xmlScore = xmlSav.CreateElement("Score");
        xmlScore.SetAttribute("score", score.ToString("F0"));

        root.AppendChild(xmlScore);
        xmlSav.Save(path);
    }
}
