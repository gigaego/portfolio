﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour {

    public float rotationX;
    public float rotationZ;

    private float auxX;
    private float auxZ;

    public float rotation_speed;

    private Quaternion rotation;

    public bool rotateX;
    public bool rotateZ;

    public void Initialized()
    {
        rotationX = Random.Range(0, 4);
        rotationZ = Random.Range(0, 4);

        rotation = Quaternion.Euler(90 * (rotationX-1), 0, 90 * (rotationZ - 1)) * transform.rotation;
        transform.rotation = rotation;

        rotateX = false;
        rotateZ = false;
    }

    // Update is called once per frame
    void Update () {
        if (rotationX > 4)
        {
            rotationX -= 4;
        }

        if (rotationZ > 4)
        {
            rotationZ -= 4;
        }

        if (rotationX < 1)
        {
            rotationX += 4;
        }

        if (rotationZ < 1)
        {
            rotationZ += 4;
        }

        #region TODO_Implementar_en_corrutina
        if (rotateX)
        {
            rotation = Quaternion.Euler(90, 0, 0) * transform.rotation;
            rotationX = auxX;
            rotateX = false;
        }
        else
        {
            auxX = rotationX + 1.0f;
        }

        if (rotateZ)
        {
            rotation = Quaternion.Euler(0, 0, 90) * transform.rotation;
            rotationZ = auxZ;
            rotateZ = false;
        }
        else
        {
            auxZ = rotationZ + 1.0f;
        } 
        #endregion

        transform.rotation = rotation;

    }
}
