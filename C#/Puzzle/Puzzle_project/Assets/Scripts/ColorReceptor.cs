﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorReceptor : MonoBehaviour {

    private int face;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<SideColor>() != null)
        {
            face = other.GetComponent<SideColor>().side;
        }
    }
    public int getSide()
    {
        return face;
    }
}
