﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBehaviour : MonoBehaviour, IInteractible {

    public bool selected;

    [SerializeField]
    private List<Dice> dices;

    public bool axisX;
    public bool axisZ;

    public GameObject shade;

    // Use this for initialization
    void Start()
    {
        selected = false;
    }
	
	public void Click () {
        if (selected)
        {
            foreach (Dice dice in dices)
            {
                if(axisX)
                    dice.rotateX = true;

                if(axisZ)
                    dice.rotateZ = true;
            }
        }
	}

    void Update()
    {
        if (selected)
            shade.SetActive(true);

        else
            shade.SetActive(false);

        if (Input.GetMouseButtonDown(0))
        {
            Click();
        }
    }

    void OnMouseOver()
    {
        selected = true;
    }

    void OnMouseExit()
    {
        selected=false;
    }
}
