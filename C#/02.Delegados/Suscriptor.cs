﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Suscriptor : MonoBehaviour
{

    public Text name;

    private EventsModule subEvents;

    private SubscriptorGenerator subGen;

    public string subName;

    public int eventID;

    private int[] probabilities;

    int maxProbability;

    void Start()
    {
        name = GameObject.FindWithTag("SubText").GetComponent<Text>();
        subEvents = GameObject.FindWithTag("MainCamera").GetComponent<EventsModule>();
        subGen = GameObject.FindWithTag("SubGen").GetComponent<SubscriptorGenerator>();
        maxProbability = subEvents.MaxProbability;
        probabilities = subEvents.probability;
        int rand = Random.Range(0, maxProbability);
        eventID = getID(rand);

        //We add the event to the delegate so AvoidNull is substituted
        subEvents.Events[eventID] = subEvents.Message;
        subEvents.Events[eventID] += AddSubscriptor;
        subEvents.Events[eventID](eventID);

    }
	
	// Update is called once per frame
	void Update () {
        Invoke("Destroy", 2);
	}

    void AddSubscriptor(int i)
    {
        Debug.Log("Subscriber " + subName);
        name.text = ("Subscriber " + subName);
    }
    void Destroy()
    {
        for (int i = 0; i < subGen.subscriptors.Count; i++)
        {
            if (subGen.subscriptors[i] == this.gameObject)
            {
                subGen.subscriptors[i] = null;
            }
        }
        
        Destroy(this.gameObject);
    }
    int getID(int rand)
    {
        int aux = 0;
        int lastChance = 0;
        for (int i = 0; i < probabilities.Length; i++)
        {
            if (rand < probabilities[0])
            {
                aux = 0;
                break;
            }
            else if (rand > lastChance && rand <= lastChance + probabilities[i])
            {
                aux = i;
            }
            lastChance += probabilities[i];
        }
        return aux;
    }
}
