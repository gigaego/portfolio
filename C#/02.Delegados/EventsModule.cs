﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EventsModule : MonoBehaviour {

    public Text eventText;

    public delegate void MyDelegate(int i);

    public string[] Messages;

    public MyDelegate Event;

    public List<MyDelegate> Events = new List<MyDelegate>();

    public int[] probability;

    private int maxProbability;

    public int MaxProbability
    {
        get
        {
            return maxProbability;
        }

        set
        {
            maxProbability = value;
        }
    }

    // Use this for initialization
    void Start () {
        MaxProbability = 0;
        foreach (int i in probability)
        {
            MaxProbability += i;
        }
        for (int i = 0; i < Messages.Length; i++)
        {
            Events.Insert(i, Event);
            Events[i] = AvoidNull;
        }
    }

    void AvoidNull(int i)
    {
        Debug.Log("El evento " +i+ " no tiene suscriptores. " + DateTime.Now);
        eventText.text = "El evento " + i+1 + " no tiene suscriptores. " + DateTime.Now;
    }

    public void Message(int i)
    {
        if (Messages[i] != null)
        {
            Debug.Log(Messages[i] + " " + DateTime.Now);
            eventText.text = Messages[i] + " " + DateTime.Now;
        }
    }

}
