﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubscriptorGenerator : MonoBehaviour
{
    
    public float timeToSpawn;
    private float timer;
    public EventsModule events;
    public GameObject subscriptorPrefab;
    public List<GameObject> subscriptors;
    public string[] names;
    private int id;
    

    // Use this for initialization
    void Start()
    {
        timer = timeToSpawn;
        id = Random.Range(0, names.Length);
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = timeToSpawn;
            for (int i = 0; i < subscriptors.Count; i++)
            {
                if (subscriptors[i] == null)
                {
                    id = Random.Range(0, names.Length);
                    subscriptorPrefab.GetComponent<Suscriptor>().subName = names[id];
                    id = Random.Range(0, names.Length);
                    subscriptors[i] = Instantiate(subscriptorPrefab);
                    break;
                }
            }
        }
    }
    
}
