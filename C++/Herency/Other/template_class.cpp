#include<iostream>
using namespace std;

template <class TP>
class geoloc{
   private:
      TP longit;
      TP latit;
   public:
      geoloc(TP longitud, TP latitud){
         longit=longitud;
         latit=latitud;
      }
      void mostrar()const{
         cout<<"Long:"<<longit<<" / "<<"Latitud:"<<latit<<endl;
      }
      void setlong(TP lon){
         longit=lon;
      }
      void setlat(TP lat){
         latit=lat;
      }
};

int main(){
    float long1 = 75.50;
    float lat1 = 25.55;
    geoloc <float> n1(long1, lat1);
    n1.mostrar();
    int long2 = 75;
    int lat2 = 25;
    geoloc <int> n2(long2, lat2);
    n2.mostrar();
    long long3=74;
    long lat3=24;
    geoloc <long> n3(long3, lat3);
    n3.mostrar();
system("PAUSE");
}
