#include <iostream>
#include <math.h>
using namespace std;
struct vector{
   int x, y;
   vector(){x=0 ; y=0;};
   void setX(int v){
      x=v;
   }
   void setY(int v){
      y=v;
   }
   int getX(){
      return x;
   }
   int getY(){
      return y;
   }
   float distancia(int x2,int y2){
      return sqrt((x2-x)*(x2-x)+(y2-y)*(y2-y));
   }
   void resta(int x2, int y2){
      cout<< "x: "<< x-x2<< "y: "<< y-y2<<endl;
   }
   int productoescalar(int x2, int y2){
      return x*y+x2*y2;
   }
   float modulo(){
      return sqrt(x*x+y*y);
   }
}vector1, vector2;

int main(){
   
   vector1.setX(2);
   vector1.setY(3);
   
   vector2.setX(1);
   vector2.setY(2);
   
   cout<<"vector 1 x: "<<vector1.getX()<<endl;
   cout<<"vector 1 y: "<<vector1.getY()<<endl;
   
   cout<<"vector 2 x: "<<vector2.getX()<<endl;
   cout<<"vector 2 y: "<<vector2.getY()<<endl;
   
   cout<<"distancia entre los vectores: "<<vector1.distancia(1,2)<<endl; 
   cout<<"resta de los vectores: ";
   vector1.resta(1,2);
   cout<<"producto escalar: "<<vector1.productoescalar(1,2)<<endl;
   cout<<"modulo vector 1: "<<vector1.modulo()<<endl;
   cout<<"modulo vector 2: "<<vector2.modulo()<<endl;
system("PAUSE");
}
