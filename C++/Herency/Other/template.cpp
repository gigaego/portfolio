#include<iostream>
#define PI 3.14
#include<string.h>
using namespace std;

template <class TP>
TP maximo(TP a, TP b, TP c){
   if(a<b&&b>c){
      return b;
   }
   else if(a<b&&b<c){
      return c;
   }
   else{
      return a;
   }
}
template <class TP>
TP area(TP base, TP altura){
   return base*altura/2;
}
template <class TP>
TP longitud(TP radio){
   return radio*2*PI;
}
template <class vec>
vec minimo(vec vector[5]){
   int aux;
   vec min;
   for(aux=0; aux<5; aux++){
      if(aux==0){
         min=vector[aux];
      }
      if(vector[aux]<min){
         min=vector[aux];
      }
   }
   return min;
}
int main(void){
   int a=10;
   int b=12;
   int f=23;
   
   float c=20.5;
   float d=20.55;
   float e=21;
   
   char ca='a';
   char cb='b';
   
   int vector[]={5,8,9,6,7};
   float vector1[]={5.1,6.1,3.1,7.5,3.12};
   char vector2[]={'a','d','i','o','s'};
   
   cout<<"El maximo entre a  b y f: "<<maximo(a, b, f)<<endl;
   cout<<"El maximo entre c  d y e: "<<maximo(c, d, e)<<endl;
   cout<<"Area triangulo base a y altura b: "<<area(a, b)<<endl;
   cout<<"Longitud de un circulo de radio a: "<<longitud(c)<<endl;
   cout<<"Minimo de los elementos del vector: "<<minimo(vector)<<endl;
   cout<<"Minimo de los elementos del vector1: "<<minimo(vector1)<<endl;
   cout<<"Minimo de los elementos del vector2: "<<minimo(vector2)<<endl;
system("PAUSE");
}
