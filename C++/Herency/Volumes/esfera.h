#include<iostream>
using namespace std;
#define PI 3.14

class esfera : public volumenes{
   private:
      double radio;
   public:
      esfera(double r){
         radio=r;
      }
      virtual double volumen(void);
      virtual double area(void);
      virtual void mostrar(void);
};
double esfera::volumen(void){
   return 4/3*PI*radio*radio*radio;
}
double esfera::area(void){
   return 4*PI*radio*radio;
}
void esfera::mostrar(void){
   cout<<"ESFERA"<<endl<<"radio: "<<radio<<endl;
}
