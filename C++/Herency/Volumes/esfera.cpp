#include<iostream>
using namespace std;
#define PI 3.14

template <class TP>
class esfera{
   private:
      TP radio;
   public:
      esfera(TP r){
         radio=r;
      }
      double volumen(){
         return 4/3*PI*radio*radio*radio;
      }
      double area(){
         return 4*PI*radio*radio;
      }
      void mostrar(){
         cout<<"ESFERA"<<endl<<"radio: "<<radio<<endl;
      }
};
int main(){
    float r1 = 10.5;
    esfera <float> n1(r1);
    n1.mostrar();
    int r2 = 10;
    esfera <int> n2(r2);
    n2.mostrar();
    long r3 = 20;
    esfera <long> n3(r3);
    n3.mostrar();
system("PAUSE");
}
