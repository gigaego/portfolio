
class volumenes{
   public:
      virtual double volumen(void)=0;
      virtual double area(void)=0;
      virtual void mostrar(void)=0;
};
