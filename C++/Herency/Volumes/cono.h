#include<iostream>
using namespace std;
#define PI 3.14

class cono : public volumenes{
   private:
      double radio;
      double altura;
      double long_lado;
   public:
      cono(double r, double h, double l){
         radio=r;
         altura=h;
         long_lado=l;
      }
      double volumen(void);
      double area(void);
      void mostrar(void);
};
double cono::volumen(void){
   return PI*radio*radio+PI*radio*long_lado;
}
double cono::area(void){
   return PI*radio*radio*altura/3;
}
void cono::mostrar(void){
   cout<<"CONO"<<endl<<"radio: "<<radio<<endl<<"altura: "<<altura<<endl<<"g: "<<long_lado<<endl;
}
