#include<iostream>
using namespace std;
#define PI 3.14

class cilindro : public volumenes{
   private:
      double radio;
      double altura;
   public:
      cilindro(double r, double h){
         radio=r;
         altura=h;
      }
      virtual double volumen(void);
      virtual double area(void);
      virtual void mostrar(void);
};
double cilindro:: volumen(void){
   return 2*PI*radio*(altura+radio);
}
double cilindro::area(void){
   return PI*radio*radio*altura;
}
void cilindro::mostrar(void){
   cout<<"CILINDRO"<<endl<<"radio: "<<radio<<endl<<"altura: "<<altura<<endl;
}
