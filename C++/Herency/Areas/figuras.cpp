#include "figura.h"
#include <iostream>
#include "triangulo.h"
#include "rectangulo.h"
#include "circulo.h"

using namespace std;

int main(void){
   triangulo p1(1,2);
   rectangulo p2(2,1);
   circulo p3(5);
   
   cout<<p3.area()<<endl;
   
   system("PAUSE");
}
