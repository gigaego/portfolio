#include <iostream>
using namespace std;

class rectangulo: public figura{
   private:
      double base;
      double altura;
   public:
      rectangulo(double b, double h);
      virtual double area(void);
      virtual void mostrar(void);
};

rectangulo::rectangulo(double b, double h){
   base=b;
   altura=h;
}

double rectangulo::area(void){
   return base*altura;
}

void rectangulo::mostrar(void){
   cout<<"El rectangulo tiene base: "<<base<<" y altura: "<<altura<<endl;    
}
