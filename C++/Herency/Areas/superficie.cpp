#include "superficies.h"
#include <iostream>
using namespace std;
int main(){
   int opcion;
   cout << "1.Circulo" << endl << "2.trapecio"<<endl;
   cin >> opcion;
   cin.ignore();
   if (opcion==1){
      double radio;
      cout << "Introduzca area: ";
      cin >> radio;
      cin.ignore();
      using namespace circulo;
      cout << "Area: ";
      cout << area(radio) << endl;
      cout << "Longitud: ";
      cout << longitud(radio) << endl;
   }
   if (opcion==2){
      using namespace trapecio;
      double basema;
      double baseme;
      double h;
      
      cout << "Introduzca base mayor: ";
      cin >> basema;
      cin.ignore();
      cout << endl;
      
      cout << "Introduzca base menor: ";
      cin >> baseme;
      cin.ignore();
      cout << endl;
      
      cout << "Introduzca altura: ";
      cin >> h;
      cin.ignore();
      cout << endl;
      cout << area(basema, baseme, h) << endl;
   }
system("PAUSE");
}
