#include <iostream>
using namespace std;
#define PI 3.14

class circulo: public figura{
   private:
      double radio;
   public:
      circulo(double r);
      virtual double area(void);
      virtual void mostrar(void);
};

circulo::circulo(double r){
   radio=r;
}

double circulo::area(void){
   return radio*PI*radio;
}

void circulo::mostrar(void){
   cout<<"El circulo tiene radio: "<<radio<<endl;    
}
