#include <iostream>
using namespace std;

class triangulo: public figura{
   private:
      double base;
      double altura;
   public:
      triangulo(double b, double h);
      virtual double area(void);
      virtual void mostrar(void);
};

triangulo::triangulo(double b, double h){
   base=b;
   altura=h;
}

double triangulo::area(void){
   return base*altura/2;
}

void triangulo::mostrar(void){
   cout<<"El triangulo tiene base: "<<base<<" y altura: "<<altura<<endl;    
}
