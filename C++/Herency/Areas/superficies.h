#define PI 3.14

namespace circulo{
   double area(double r){
      return PI*r*r;
   }
   double longitud(double r){
      return PI*2*r;
   }
}

namespace trapecio{
   double area(double b,double B, double h){
      return (b+B)/2*h;
   }
}
