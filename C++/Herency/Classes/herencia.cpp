#include <iostream>
#include <string.h>
using namespace std;

class persona{
   private:
      char nombre[20];
      char apellido[20];
      char DNI[10];
      char genero[10];
      char estado_civil[10];
      int edad;
   public:
      void set_nombre(char name[20]){
         strcpy(nombre, name);
      }
      void set_apellido(char surname[20]){
         strcpy(apellido, surname);
      }
      void set_DNI(char dni[10]){
         strcpy(DNI, dni);
      }
      void set_genero(char gen[10]){
         strcpy(genero, gen);
      }
      void set_estado(char estado[10]){
         strcpy(estado_civil, estado);
      }
      void set_edad(int old){
         edad=old;
      }
      void get_nombre(char name[20])const{
         strcpy(name, nombre);
      }
      void get_apellido(char surname[20])const{
         strcpy(surname, apellido);
      }
      void get_DNI(char dni[10])const{
         strcpy(dni, DNI);
      }
      void get_genero(char gen[10])const{
         strcpy(gen, genero);
      }
      void get_estado(char estado[10])const{
         strcpy(estado, estado_civil);
      }
      void get_edad(int old)const{
         old=edad;
      }
};

class alumno: public persona{
   private:
      char titulacion[50];
      int curso;
   public:
      void set_titulacion(char titulo[50]){
         strcpy(titulacion, titulo);
      }
      void set_curso(int course){
         curso=course;
      }
      void get_titulacion(char titulo[50]){
         strcpy(titulo, titulacion);
      }
      void get_curso(int course){
         course=curso;
      }
};

int main(){
   alumno a01;
   char name[20], surname[20], dni[10], gen[10], estado[10], titulo[50];
   int old, course;
   a01.set_nombre("Jaimito");
   a01.get_nombre(name);
   cout<<name<<endl;
system("PAUSE");
}
