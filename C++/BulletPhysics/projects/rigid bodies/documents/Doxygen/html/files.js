var files =
[
    [ "Entity.cpp", "_entity_8cpp.html", null ],
    [ "Entity.hpp", "_entity_8hpp.html", [
      [ "Entity", "classpr02_1_1_entity.html", "classpr02_1_1_entity" ]
    ] ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "Platform.cpp", "_platform_8cpp.html", null ],
    [ "Platform.hpp", "_platform_8hpp.html", [
      [ "Platform", "classpr02_1_1_platform.html", "classpr02_1_1_platform" ]
    ] ],
    [ "Player.cpp", "_player_8cpp.html", null ],
    [ "Player.hpp", "_player_8hpp.html", [
      [ "Player", "classpr02_1_1_player.html", "classpr02_1_1_player" ]
    ] ],
    [ "Stairs.cpp", "_stairs_8cpp.html", null ],
    [ "Stairs.hpp", "_stairs_8hpp.html", [
      [ "Stairs", "classpr02_1_1_stairs.html", "classpr02_1_1_stairs" ]
    ] ],
    [ "World.cpp", "_world_8cpp.html", null ],
    [ "World.hpp", "_world_8hpp.html", [
      [ "World", "classpr02_1_1_world.html", "classpr02_1_1_world" ]
    ] ]
];