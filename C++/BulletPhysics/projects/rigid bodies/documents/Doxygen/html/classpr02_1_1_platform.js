var classpr02_1_1_platform =
[
    [ "Platform", "classpr02_1_1_platform.html#a3dd0dcb78312302a25a661fb9d97415e", null ],
    [ "Platform", "classpr02_1_1_platform.html#a01705d7299e4370945cbcaf6060197cf", null ],
    [ "~Platform", "classpr02_1_1_platform.html#a40daff1193a55d989af7e55803f2d019", null ],
    [ "activate", "classpr02_1_1_platform.html#aff346a8fdf7f75d764e99011a2769d6d", null ],
    [ "begin_checking", "classpr02_1_1_platform.html#a529824d332fd06605a436c9a5ffd48ee", null ],
    [ "deactivate", "classpr02_1_1_platform.html#a0f662bce427b089626a11e4a954654cb", null ],
    [ "init_static_box_body", "classpr02_1_1_platform.html#a32bd0356743ff5ac0a0ccb605a646f59", null ],
    [ "move", "classpr02_1_1_platform.html#a1b08c3ec83bff090ba755e79f6a1be80", null ],
    [ "on_range", "classpr02_1_1_platform.html#a80cea2201f1e056a4e461b0c9adf724a", null ],
    [ "set_trigger", "classpr02_1_1_platform.html#aa38360d2c37ca76dcef6bc26f2b083d3", null ],
    [ "update", "classpr02_1_1_platform.html#aa601f00a3625669bb39252cfc932efa0", null ]
];