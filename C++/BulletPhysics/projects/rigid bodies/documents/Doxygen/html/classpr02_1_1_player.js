var classpr02_1_1_player =
[
    [ "Player", "classpr02_1_1_player.html#af02257f4e897913a3fe5b4419fc982b8", null ],
    [ "~Player", "classpr02_1_1_player.html#a85f1a18a937c003db18607c466b35c60", null ],
    [ "do_break_speed", "classpr02_1_1_player.html#a9c628239ec27cb825798e669c517d52a", null ],
    [ "dont_break_speed", "classpr02_1_1_player.html#ab223000210670bd61110a1dc69136f71", null ],
    [ "get_key", "classpr02_1_1_player.html#a6792239aa875489dd02995db69efad13", null ],
    [ "got_key", "classpr02_1_1_player.html#a8f5a382e8feb996b4f149032ad37875a", null ],
    [ "jump", "classpr02_1_1_player.html#a70e21ca98281b7d72f105f2693113d7e", null ],
    [ "move_left", "classpr02_1_1_player.html#ac085ca03022db758defe9e8d44af0691", null ],
    [ "move_right", "classpr02_1_1_player.html#ae565fd6f1c3c293fae5cd52d692deb9f", null ],
    [ "moving", "classpr02_1_1_player.html#af15c8fcb7013401fc0f2c6e4469d28dd", null ],
    [ "reset_jump", "classpr02_1_1_player.html#a150eeca2b3912149bef0c0d780e79db9", null ],
    [ "stop_move", "classpr02_1_1_player.html#a6787f55aaea2e30bc16c5e20fe5834c6", null ],
    [ "update", "classpr02_1_1_player.html#a6912bb6e48efb5845d59f0f4582827ef", null ]
];