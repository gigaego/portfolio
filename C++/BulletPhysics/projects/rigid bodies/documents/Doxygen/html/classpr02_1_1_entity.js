var classpr02_1_1_entity =
[
    [ "Entity", "classpr02_1_1_entity.html#ad278357380b503488043fd833691a7f2", null ],
    [ "Entity", "classpr02_1_1_entity.html#aa97c2f8a6a5237eda1eb7b305bca4ad9", null ],
    [ "get_body", "classpr02_1_1_entity.html#a812970998934bf64ef7fcfca7370a85b", null ],
    [ "get_transform", "classpr02_1_1_entity.html#a5db43cdc7956cc72448792ec65504cb3", null ],
    [ "init_dynamic_sphere_body", "classpr02_1_1_entity.html#a0fdbf6d966367567a971d20a661dd8d4", null ],
    [ "init_static_box_body", "classpr02_1_1_entity.html#a58b15e878f2ae5c066b0acb53b377753", null ],
    [ "scale", "classpr02_1_1_entity.html#ab01ca2974dca66bf637d616c001fa844", null ],
    [ "scale", "classpr02_1_1_entity.html#ae4968849f5551d9dfb884186b964468d", null ],
    [ "set_transform", "classpr02_1_1_entity.html#a02d5e4a50332e8ccedf6bc366ef473e6", null ],
    [ "update", "classpr02_1_1_entity.html#a00b6eeaf99b35c8f8b10b5fbfc1baf4f", null ],
    [ "body", "classpr02_1_1_entity.html#a2225abac413204e5df6b5b83f7cccee7", null ],
    [ "mesh", "classpr02_1_1_entity.html#afbcbaf7a0170cb4682d08fb8e73a57ab", null ],
    [ "scale_x", "classpr02_1_1_entity.html#a9aeb209b881bddab306c6593c9846afb", null ],
    [ "scale_y", "classpr02_1_1_entity.html#a1e379de8572c40429909ccd8050b161f", null ],
    [ "scale_z", "classpr02_1_1_entity.html#af69b2bf4ecc030999985f175b9524d23", null ],
    [ "this_transform", "classpr02_1_1_entity.html#a3aed6a19ebcc1eafa0eab2cb044910c7", null ]
];