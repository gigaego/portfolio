#ifndef PLATFORM_HEADER
#define PLATFORM_HEADER

#include "Entity.hpp"
namespace pr02
{
	/**
	* @class Platform
	* @brief Entity that moves when activated and returns to his position when deactivated
	*/
	class Platform : public Entity
	{
	public:
		/**
		* Platform constructor receives the height limits and his Y movement speed
		*/
		Platform(Scene_Object * model, float max_height, float min_height, float speed, bool horizontal);
		Platform(Scene_Object * model, float max_height, float min_height, float speed, float dist, bool horizontal);
		~Platform() {}

	private:
		bool horizontal;
		bool  activated;
		bool  pos01;
		bool  pos02;
		float max_pos;
		float min_pos;
		float max_speed;
		float speed;
		float sqr_dist_trigger;
		bool  check_dist;

		btTransform * trigger;

	public:
		/**
		* Call to activate the platform's movement
		*/
		void activate()
		{
			activated = true;
		}
		/**
		* Call to stop the platform's movement
		*/
		void deactivate()
		{
			activated = false;
		}

		void move(btTransform *transform);

		void on_range();

		void set_trigger(btTransform * transform)
		{
			trigger = transform;
		}

		void update();

		void begin_checking()
		{
			check_dist = true;
		}

		void init_static_box_body
		(
			float col_size_x, float col_size_y, float col_size_z,
			float origin_x, float origin_y, float origin_z,
			World * world
		);

	};

}
#endif // !PLATFORM_HEADER
