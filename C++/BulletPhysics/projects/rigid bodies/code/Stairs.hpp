#ifndef STAIRS_HEADER
#define STAIRS_HEADER
#include "Entity.hpp"

namespace pr02
{
	/**
	* @class Stairs
	* @brief this class is pretty similar to a normal Entity but the transform has been rotated
	*/
	class Stairs :public Entity
	{
	public:
		Stairs(Scene_Object * model, bool right);
		~Stairs() {}

	private:
		btTransform graphic_pos;
		bool right;
		float pos_x;
		float pos_y;
		float pos_z;

	public:
		void update() override;
		void init_static_box_body
		(
			float col_size_x, float col_size_y, float col_size_z,
			float origin_x, float origin_y, float origin_z,
			World * world
		);

		/**
		* if the hypotenuse is 1 
		* then the leg is ~0.707
		*/
		void scale() override
		{
			mesh->scale(scale_x * 0.707f, scale_y * 0.707f, scale_z * 0.707f);
		}

	};

}
#endif // !STAIRS_HEADER
