#include "Entity.hpp"

using namespace pr02;

/**
* Initialize a static body with a box shape
* and add it to the world
*/
void Entity::init_static_box_body
(
	float col_size_x, float col_size_y, float col_size_z,
	float origin_x, float origin_y, float origin_z,
	World * world
)
{
	std::shared_ptr< btCollisionShape > shape(new btBoxShape(btVector3(col_size_x, col_size_y, col_size_z)));

	scale_x = col_size_x;
	scale_y = col_size_y;
	scale_z = col_size_z;

	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(btVector3(origin_x, origin_y, origin_z));

	std::shared_ptr< btDefaultMotionState >       state(new btDefaultMotionState(transform));
	btRigidBody::btRigidBodyConstructionInfo info(0, state.get(), shape.get());
	std::shared_ptr< btRigidBody >                n_body(new btRigidBody(info));
	body = n_body;

	// Add the body to the dynamics world.
	world->get_dynamicsWorld()->addRigidBody(body.get());

	// Save the smart pointers for automatic cleanup.
	world->push_back(body);
	world->push_back(state);
	world->push_back(shape);
}

/**
* Initialize a static body with a sphere shape
* and add it to the world
*/
void Entity::init_dynamic_sphere_body
(
	float radius,
	float origin_x, float origin_y, float origin_z,
	World * world
)
{
	scale_x = 1.f;
	scale_y = 1.f;
	scale_z = 1.f;

	shared_ptr< btCollisionShape > shape(new btSphereShape(radius));

	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(btVector3(origin_x, origin_y, origin_z));

	btScalar  mass = 5.f;
	btVector3 localInertia(0, 0, 0);

	shared_ptr< btDefaultMotionState >       state(new btDefaultMotionState(transform));
	btRigidBody::btRigidBodyConstructionInfo info(mass, state.get(), shape.get(), localInertia);
	shared_ptr< btRigidBody >                n_body(new btRigidBody(info));
	body = n_body;

	// The body won't fall asleep
	body->setActivationState(DISABLE_DEACTIVATION);

	// The body won't rotate
	body->setAngularFactor(btVector3(0, 0, 0));

	// The body won't change the z value
	body->setLinearFactor(btVector3(1, 1, 0));

	world->get_dynamicsWorld()->addRigidBody(body.get());

	// Save the smart pointers for automatic cleanup.

	world->push_back(body);
	world->push_back(state);
	world->push_back(shape);
}

/**
* This function gets the physic transform
* and applies it to the graphic transform
*/
void Entity::update()
{
	float mat[16];

	this_transform->getOpenGLMatrix(mat);
	mesh->update_btTransform(mat);
	scale();
}