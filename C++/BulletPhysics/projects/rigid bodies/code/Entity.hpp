#ifndef ENTITY_HEADER
#define ENTITY_HEADER

#include <memory>
#include <Scene.hpp>
#include "World.hpp"
#include <iostream>

using namespace toolkit;

namespace pr02
{
	/**
	* @class Entity
	* @brief This class manages the physic and graphic part
	*/
	class Entity
	{
	protected:
		Scene_Object					  * mesh;
		std::shared_ptr< btRigidBody >      body;
		float scale_x, scale_y, scale_z;
		btTransform						  *	this_transform;
	public:
		Entity() { }

		Entity(Scene_Object * model) : mesh(model) { }

		/**
		* The first 3 parameters are the sizes of the collider
		* The next 3 parameters are the position in which is created
		*/
		virtual void init_static_box_body
		(
			float col_size_x, float col_size_y, float col_size_z,
			float origin_x, float origin_y, float origin_z,
			World * world
		);

		/**
		* The first parameter is the collider radius
		* and the next 3 parameters are the position in which is created
		*/
		void init_dynamic_sphere_body
		(
			float radius,
			float origin_x, float origin_y, float origin_z,
			World * world
		);

		std::shared_ptr<btRigidBody> get_body() const { return body; }

		/**
		* This function receives the world
		* and the number in the CollisionObjectArray
		*/
		virtual void update();
		
		virtual void scale() { mesh->scale(scale_x, scale_y, scale_z); }

		virtual void scale(float x, float y, float z) { scale_x = x; scale_y = y; scale_z = z; }

		void set_transform(btTransform * transform)
		{
			this_transform = transform;
		}

		btTransform * get_transform()
		{
			return this_transform;
		}
	};
	
}

#endif // !ENTITY_HEADER