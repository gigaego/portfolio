#include "Platform.hpp"

using namespace pr02;

Platform::Platform(Scene_Object * model, float max_pos, float min_pos, float speed, bool horizontal)
	: max_pos(max_pos), min_pos(min_pos), max_speed(speed), horizontal(horizontal)
{
	mesh = model;
	activated = false;
	pos01 = false;
	pos02 = false;
	speed = 0;

	trigger = nullptr;
	sqr_dist_trigger = 0;
	check_dist = false;

}

pr02::Platform::Platform(Scene_Object * model, float max_pos, float min_pos, float speed, float dist, bool horizontal)
	: max_pos(max_pos), min_pos(min_pos), max_speed(speed), horizontal(horizontal)
{
	mesh = model;
	activated = false;
	pos01 = false;
	pos02 = false;
	speed = 0;

	trigger = nullptr;
	sqr_dist_trigger = dist;
	check_dist = false;
}

/**
* This function checks whether the platform 
* has reached a limit or not and changes his 
* direction
*/
void Platform::move(btTransform * transform)
{
	if (!horizontal)
	{
		if (transform->getOrigin().getY() <= max_pos)
		{
			speed = abs(max_speed);
		}

		if (transform->getOrigin().getY() > max_pos)
		{
			speed = 0;
		}

		get_body()->setLinearVelocity(btVector3(0, speed, 0));
	}

	else if (horizontal)
	{
		if (transform->getOrigin().getX() <= min_pos)
		{
			speed = abs(max_speed);
		}

		if (transform->getOrigin().getX() > max_pos)
		{
			speed = abs(max_speed) * -1;
		}

		get_body()->setLinearVelocity(btVector3(speed, 0, 0));
	}
}

void Platform::on_range()
{
	float x = this_transform->getOrigin().getX();
	float y = this_transform->getOrigin().getY();

	float my_x = trigger->getOrigin().getX();
	float my_y = trigger->getOrigin().getY();

	x = my_x - x;
	y = my_y - y;
	float sqr_dist = x*x + y*y;

	if (sqr_dist < sqr_dist_trigger)
	{
		activate();
	}
	else
	{
		deactivate();
	}

}

/**
* Similar update to a normal Entity but
* it changes direction when a limit is reached
*/
void Platform::update()
{
	float				mat[16];
	if (!activated)
	{
		get_body()->setLinearVelocity(btVector3(0, 0, 0));
	}

	this_transform->getOpenGLMatrix(mat);
	mesh->update_btTransform(mat);
	scale();
	if (!horizontal)
	{
		if (activated)
		{
			move(this_transform);
		}
		if (!activated && this_transform->getOrigin().getY() > min_pos)
		{
			max_speed = abs(max_speed);
			max_speed *= -1;
			get_body()->setLinearVelocity(btVector3(0, max_speed, 0));
		}

		if (check_dist && trigger != nullptr)
		{
			on_range();
		}
	}

	else if (horizontal)
	{
		activate();
		if (activated)
		{
			move(this_transform);
		}

		if (check_dist && trigger != nullptr)
		{
			on_range();
		}
	}

}

/**
* Initialize a static body with a box shape
* and add it to the world
*/

void Platform::init_static_box_body
(
	float col_size_x, float col_size_y, float col_size_z, 
	float origin_x, float origin_y, float origin_z,
	World * world
)
{
	std::shared_ptr< btCollisionShape > shape(new btBoxShape(btVector3(col_size_x, col_size_y, col_size_z)));

	scale_x = col_size_x;
	scale_y = col_size_y;
	scale_z = col_size_z;

	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(btVector3(origin_x, origin_y, origin_z));

	btScalar  mass = 5.f;
	btVector3 localInertia(0, 0, 0);

	shape->calculateLocalInertia(mass, localInertia);

	shared_ptr< btDefaultMotionState >       state(new btDefaultMotionState(transform));
	btRigidBody::btRigidBodyConstructionInfo info(mass, state.get(), shape.get(), localInertia);
	shared_ptr< btRigidBody >                n_body(new btRigidBody(info));
	body = n_body;

	// The body won't fall asleep
	body->setActivationState(DISABLE_DEACTIVATION);

	// The body won't rotate
	body->setAngularFactor(btVector3(0, 0, 0));

	// The body won't change the z value
	if(!horizontal)
		body->setLinearFactor(btVector3(0, 1, 0));
	else if(horizontal)
		body->setLinearFactor(btVector3(1, 0, 0));

	// Add the body to the dynamics world.
	world->get_dynamicsWorld()->addRigidBody(body.get());

	// Save the smart pointers for automatic cleanup.
	world->push_back(body);
	world->push_back(state);
	world->push_back(shape);

	get_body()->setGravity(btVector3(0, 0, 0));
}
