#include "Stairs.hpp"

using namespace pr02;

Stairs::Stairs(Scene_Object * model, bool right) : right(right)
{
	mesh = model;

	pos_x = 0;
	pos_y = 0;
	pos_z = 0;

}

void Stairs::init_static_box_body(float col_size_x, float col_size_y, float col_size_z, float origin_x, float origin_y, float origin_z, World * world)
{
	std::shared_ptr< btCollisionShape > shape(new btBoxShape(btVector3(col_size_x, col_size_y, col_size_z)));

	scale_x = col_size_x;
	scale_y = col_size_y;
	scale_z = col_size_z;

	btTransform transform;
	btQuaternion rot = btQuaternion(0, 0, 45);
	transform.setIdentity();
	transform.setOrigin(btVector3(origin_x, origin_y, origin_z));
	transform.setRotation(rot);

	pos_x = origin_x;
	pos_y = origin_y;
	pos_z = origin_z;

	std::shared_ptr< btDefaultMotionState >       state(new btDefaultMotionState(transform));
	btRigidBody::btRigidBodyConstructionInfo info(0, state.get(), shape.get());
	std::shared_ptr< btRigidBody >                n_body(new btRigidBody(info));
	body = n_body;

	// Add the body to the dynamics world.
	world->get_dynamicsWorld()->addRigidBody(body.get());

	// Save the smart pointers for automatic cleanup.
	world->push_back(body);
	world->push_back(state);
	world->push_back(shape);
}

void Stairs::update()
{

	float				mat[16];
	btTransform         transform;

	transform = *this_transform;
	btQuaternion rot = btQuaternion(0, 0, 0);
	transform.setRotation(rot);

	// The new floats are the position of the rendered stair
	// But the collider 
	float new_x;

	if (right)
		new_x = pos_x - scale_x;

	else
		new_x =  pos_x + scale_x;

	float new_y = pos_y + scale_y;

	transform.setOrigin(btVector3( new_x, new_y, pos_z));
	transform.getOpenGLMatrix(mat);
	mesh->update_btTransform(mat);

	scale();
}





