#ifndef PLAYER_HEADER
#define PLAYER_HEADER	

#include "Entity.hpp"

namespace pr02
{
	/**
	* @class Player
	* @brief This class is like any other Entity but it moves with inputs
	*/
	class Player : public Entity
	{
	private:

		bool jump_avalaible;
		bool jumping;
		bool right;
		bool left;
		// set true to show position
		bool debug_pos;
		// set true if the key has been grabbed
		bool key;
		bool break_speed;

	public:
		Player(Scene_Object * model);
		
		~Player() {}

	public:

		/**
		* Resets the jump 
		*/
		void reset_jump();

		void jump();

		/**
		* This function makes you fall faster
		* but eliminates any other kind of movement
		*/
		void stop_move()
		{
			get_body()->setLinearVelocity(btVector3(0, -10, 0));
		}

		void move_right()
		{
			get_body()->setLinearVelocity(btVector3(10, get_body()->getLinearVelocity().y(), 0));
		}

		void move_left()
		{
			get_body()->setLinearVelocity(btVector3(-10, get_body()->getLinearVelocity().y(), 0));
		}

		void update() override;

		bool get_key() { return key; }

		void got_key() { key = true; }

		void dont_break_speed()
		{
			break_speed = false;
		}

		void do_break_speed()
		{
			break_speed = true;
		}

		bool moving()
		{
			if (!jumping && !right && !left)
			{
				if(break_speed)
					get_body()->setLinearVelocity(btVector3(0, get_body()->getLinearVelocity().y(), 0));
				return false;
			}
				
			else
				return true;
		}

	};
}
#endif // !PLAYER_HEADER
