/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Started by �ngel on april of 2016                                          *
*                                                                             *
*  This is free software released into the public domain.                     *
*                                                                             *
*  angel.rodriguez@esne.edu                                                   *
*																			  *
*  Modified by Jiale Cheng													  *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <iostream>

#include "Platform.hpp"
#include "Stairs.hpp"
#include "Player.hpp"
#include "World.hpp"


#include <SFML/Window.hpp>

#include <glbinding/gl/gl.h>
#include <glbinding/Binding.h>

using namespace std;
using namespace pr02;


namespace
{
    shared_ptr< Scene > create_scene()
    {
        shared_ptr< Scene  > scene(new Scene);
        shared_ptr< Model  > player(new Model);
        shared_ptr< Model  > ground(new Model);
		shared_ptr< Model  > roof(new Model);
		shared_ptr< Model  > key(new Model);
		shared_ptr< Model  > left_wall(new Model);
		shared_ptr< Model  > right_wall(new Model);
		shared_ptr< Model  > step01(new Model);
		shared_ptr< Model  > step02(new Model);
		shared_ptr< Model  > step03(new Model);
		shared_ptr< Model  > step04(new Model);
		shared_ptr< Model  > step05(new Model);
		shared_ptr< Model  > ground2(new Model);
		shared_ptr< Model  > ground3(new Model);
		shared_ptr< Model  > platform(new Model);
		shared_ptr< Model  > door(new Model);
		shared_ptr< Model  > hole(new Model);
		shared_ptr< Model  > platform2(new Model);
        shared_ptr< Camera > camera(new Camera(20.f, 1.0f, 100.f, 1.f));
        shared_ptr< Light  > light(new Light);

        player->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
        ground->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
		roof->		add(shared_ptr< Drawable >(new Cube), Material::default_material());
		key->		add(shared_ptr< Drawable >(new Cube), Material::default_material());
		left_wall-> add(shared_ptr< Drawable >(new Cube), Material::default_material());
		right_wall->add(shared_ptr< Drawable >(new Cube), Material::default_material());
		step01->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
		step02->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
		step03->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
		step04->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
		step05->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
		ground2->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
		ground3->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
		platform->	add(shared_ptr< Drawable >(new Cube), Material::default_material());
		door->		add(shared_ptr< Drawable >(new Cube), Material::default_material());
		hole->		add(shared_ptr< Drawable >(new Cube), Material::default_material());
		platform2-> add(shared_ptr< Drawable >(new Cube), Material::default_material());

        scene->add("player",  player);
        scene->add("ground", ground);
		scene->add("roof", roof);
		scene->add("key", key);
		scene->add("leftWall", left_wall);
		scene->add("rightWall", right_wall);
        scene->add("camera", camera);
        scene->add("light",  light);
		scene->add("step01", step01);
		scene->add("step02", step02);
		scene->add("step03", step03);
		scene->add("step04", step04);
		scene->add("step05", step05);
		scene->add("ground2", ground2);
		scene->add("ground3", ground3);
		scene->add("platform", platform);
		scene->add("door", door);
		scene->add("hole", hole);
		scene->add("platform2", platform2);

        return scene;
    }

    void configure_scene(Scene & scene)
    {
        scene["light"]->translate(Vector3(0.f, 10.f, -10.f));
    }

    void reset_viewport(const sf::Window & window, Scene & scene)
    {
        GLsizei  width = GLsizei(window.getSize().x);
        GLsizei  height = GLsizei(window.getSize().y);

        scene.get_active_camera()->set_aspect_ratio(float(width) / height);

        glViewport(0, 0, width, height);
    }

}

int main ()
{
	// We create the World and we initialize it
	World world;

	sf::Window window
	(
		sf::VideoMode(800, 600),
		"Plataformas",
		sf::Style::Default,
		sf::ContextSettings(24, 0, 0, 3, 2, sf::ContextSettings::Core)      // Se usa OpenGL 3.2 core profile
	);

	// Se determinan las caracter�sticas de OpenGL disponibles en la m�quina:

	glbinding::Binding::initialize();

	// Se activa la sincronizaci�n vertical:

	window.setVerticalSyncEnabled(true);

	// Se crea y se configura la escena:

	shared_ptr< Scene > scene = create_scene();

	vector< Entity *> entities;

	configure_scene(*scene);

	// We create the entities and add them to the world
	// and to entities
	#pragma region create_entities
	Player player(scene->get("player"));
	player.init_dynamic_sphere_body(1.15f, -35, -20, -50, &world);
	entities.push_back(&player);

	Entity ground(scene->get("ground"));
	ground.init_static_box_body(40.f, 1.f, 1.f, 0, -30, -50, &world);
	entities.push_back(&ground);

	Entity ground2(scene->get("ground2"));
	ground2.init_static_box_body(18.2f, 0.7f, 1.f, 22, -24.25f, -50, &world);
	entities.push_back(&ground2);

	Entity ground3(scene->get("ground3"));
	ground3.init_static_box_body(10.2f, 0.7f, 1.f, -30, 10.f, -50, &world);
	entities.push_back(&ground3);

	Platform platform(scene->get("platform"), 15.f, -18.f, 8.f, false);
	platform.init_static_box_body(5.f, 0.7f, 1.f, -7.f, -18.f, -50, &world);
	entities.push_back(&platform);

	Entity key(scene->get("key"));
	key.init_static_box_body(0.5f, 0.5f, 0.5f, -30, 20.f, -50, &world);
	entities.push_back(&key);

	Platform door(scene->get("door"), -15.f, -19.5f, 15.f, 250, false);
	door.init_static_box_body(4.f, 4.f, 0.5f, 30, -19.5, -50, &world);
	entities.push_back(&door);

	Platform platform2(scene->get("platform2"), -7.f, -22.f, 5.f, true);
	platform2.init_static_box_body(5.f, 0.7f, 1.f, -22.f, -24.25f, -50, &world);
	entities.push_back(&platform2);

	Entity hole(scene->get("hole"));
	hole.init_static_box_body(4.f, 4.f, 0.5f, 35, -24, -58, &world);
	entities.push_back(&hole);

	Entity roof(scene->get("roof"));
	roof.init_static_box_body(41.f, 1.f, 1.f, 0, 22, -50, &world);
	entities.push_back(&roof);

	Entity left_wall(scene->get("leftWall"));
	left_wall.init_static_box_body(1.f, 26.f, 1.f, -40, -5, -50, &world);
	entities.push_back(&left_wall);

	Entity right_wall(scene->get("rightWall"));
	right_wall.init_static_box_body(1.f, 26.f, 1.f, 40, -5, -50, &world);
	entities.push_back(&right_wall);

	Stairs step01(scene->get("step01"), true);
	step01.init_static_box_body(1.f, 1.f, 2.f, 0, -29, -50, &world);
	entities.push_back(&step01);

	Stairs step02(scene->get("step02"), true);
	step02.init_static_box_body(1.f, 1.f, 2.f, 1, -28, -50, &world);
	entities.push_back(&step02);

	Stairs step03(scene->get("step03"), true);
	step03.init_static_box_body(1.f, 1.f, 2.f, 2, -27, -50, &world);
	entities.push_back(&step03);

	Stairs step04(scene->get("step04"), true);
	step04.init_static_box_body(1.f, 1.f, 2.f, 3, -26, -50, &world);
	entities.push_back(&step04);

	Stairs step05(scene->get("step05"), true);
	step05.init_static_box_body(1.f, 1.f, 2.f, 4, -25, -50, &world);
	entities.push_back(&step05);

	#pragma endregion

	// Se inicializan algunos elementos de OpenGL:

	reset_viewport(window, *scene);

	glClearColor(0.1f, 0.1f, 0.1f, 1.f);

	for (unsigned int i = 0; i < entities.size(); i++) 
	{
		btCollisionObject * object = world.get_dynamicsWorld()->getCollisionObjectArray()[i];
		entities[i]->set_transform(&object->getWorldTransform());
	}

	// Bucle principal:

	bool running = true;

	// A reference to the player transform is given to the door
	door.set_trigger(player.get_transform());
	do
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
			{
				running = false;
				break;
			}

			case sf::Event::Resized:
			{
				reset_viewport(window, *scene);
				break;
			}
			}
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		// Links OpenGL trnsform with btTransform
		{
			for (unsigned int i = 0; i < entities.size(); i++)
			{
				if (player.get_key() && i==6)
				{
					door.begin_checking();
				}
				entities[i]->update();
			}
		}

		world.get_dynamicsWorld()->stepSimulation(1.f / 60.f);

		scene->render();
		
		window.display();

		int numManifolds = world.get_dynamicsWorld()->getDispatcher()->getNumManifolds();
		for (int i = 0; i < numManifolds; i++)
		{
			btPersistentManifold* contactManifold = world.get_dynamicsWorld()->getDispatcher()->getManifoldByIndexInternal(i);
			const btCollisionObject* obA = contactManifold->getBody0();
			const btCollisionObject* obB = contactManifold->getBody1();

			#pragma region check_collisions
			if (
				obA == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obB == world.get_dynamicsWorld()->getCollisionObjectArray()[1]
				|| obB == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obA == world.get_dynamicsWorld()->getCollisionObjectArray()[1]
				)
			{
				player.reset_jump();
			}
			if (
				obA == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obB == world.get_dynamicsWorld()->getCollisionObjectArray()[2]
				|| obB == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obA == world.get_dynamicsWorld()->getCollisionObjectArray()[2]
				)
			{
				player.reset_jump();
			}
			if (
				obA == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obB == world.get_dynamicsWorld()->getCollisionObjectArray()[3]
				|| obB == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obA == world.get_dynamicsWorld()->getCollisionObjectArray()[3]
				)
			{
				player.reset_jump();
			}

			// Activate the platform when the player collides with it
			if (
				obA == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obB == world.get_dynamicsWorld()->getCollisionObjectArray()[4]
				|| obB == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obA == world.get_dynamicsWorld()->getCollisionObjectArray()[4]
				)
			{
				player.reset_jump();
				if (!player.moving())
				{
					player.get_body()->setLinearVelocity(
						btVector3(
							0,
							platform2.get_body()->getLinearVelocity().y(),
							0
						)

					);
				}
				platform.activate();
			}
			else
			{
				platform.deactivate();
			}

			if (
				obA == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obB == world.get_dynamicsWorld()->getCollisionObjectArray()[5]
				|| obB == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obA == world.get_dynamicsWorld()->getCollisionObjectArray()[5]
				)
			{
				if (player.get_key() == false)
				{
					player.got_key();
					key.scale(0, 0, 0);
					cout << "key: " << player.get_key() << endl;
				}
			}

			if (
				obA == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obB == world.get_dynamicsWorld()->getCollisionObjectArray()[7]
				|| obB == world.get_dynamicsWorld()->getCollisionObjectArray()[0]
				&& obA == world.get_dynamicsWorld()->getCollisionObjectArray()[7]
				)
			{
				player.reset_jump();
				if (!player.moving())
				{
					player.dont_break_speed();
					player.get_body()->setLinearVelocity(
						btVector3(
							platform2.get_body()->getLinearVelocity().x(),
							0,
							0
						)

					);
				}
			}

			else
			{
				player.do_break_speed();
			}

			#pragma endregion
		}

	} while (running);

	return (EXIT_SUCCESS);

	cin.get();

    return 0;
}