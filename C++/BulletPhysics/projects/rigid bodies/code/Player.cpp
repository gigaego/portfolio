#include "Player.hpp"
#include <Windows.h>

using namespace pr02;

/**
* The constructor initializes all booleans to false
*/
Player::Player(Scene_Object * model)
{
	mesh = model;
	jump_avalaible = true;
	jumping = false;
	right = false;
	left = false;
	debug_pos = false;
	key = false;
	break_speed = true;
}

/**
* Resets the jump
*/
void Player::reset_jump() {
	if (jump_avalaible == false)
	{
		jump_avalaible = true;
		jumping = false;
	}
}

void Player::jump()
{
	if (jump_avalaible)
	{
		jump_avalaible = false;
		get_body()->setLinearVelocity(btVector3(get_body()->getLinearVelocity().x(), 12, 0));
	}
}

/**
* This update does the same as Entity's update
* but takes into account the movement
*/
void Player::update()
{
	float				mat[16];

	// Inputs
	{
		if (GetAsyncKeyState(VK_UP))
		{
			jumping = true;
		}
		else if(!GetAsyncKeyState(VK_UP))
		{
			jumping = false;
		}

		if (GetAsyncKeyState(VK_DOWN))
		{
			stop_move();
		}

		if (GetAsyncKeyState(VK_RIGHT))
		{
			right = true;
		}

		else if (!GetAsyncKeyState(VK_RIGHT))
		{
			right = false;
		}

		if (GetAsyncKeyState(VK_LEFT))
		{
			left = true;
		}

		else if (!GetAsyncKeyState(VK_LEFT))
		{
			left = false;
		}

		if (GetAsyncKeyState(VK_F1))
		{
			debug_pos = !debug_pos;
		}
	}

	if (jumping)
	{
		jump();
	}

	if (right)
	{
		move_right();
	}

	if (left)
	{
		move_left();
	}

	moving();

	this_transform->getOpenGLMatrix(mat);
	mesh->update_btTransform(mat);
	scale();

	if (debug_pos)
	{
		cout << " X: " << this_transform->getOrigin().getX() << endl;
		cout << " Y: " << this_transform->getOrigin().getY() << endl;
		cout << " Z: " << this_transform->getOrigin().getZ() << endl;
	}
}
