#include "World.hpp"
using namespace pr02;

void World::init()
{
	collisionDispatcher = new btCollisionDispatcher(&collisionConfiguration);
	dynamicsWorld = new btDiscreteDynamicsWorld
	(
		collisionDispatcher,
		&overlappingPairCache,
		&constraintSolver,
		&collisionConfiguration
	);

	set_gravity(btVector3(0, -10, 0));
}

