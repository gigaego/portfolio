#ifndef WORLD_HEADER
#define WORLD_HEADER

#include <vector>
#include <memory>
#include <btBulletDynamicsCommon.h>
#include <Light.hpp>
#include <Camera.hpp>
#include <Model.hpp>
#include <Cube.hpp>
#include <Scene.hpp>

using namespace std;
using namespace toolkit;

namespace pr02
{
	/**
	* @class World
	* @brief This class creates and manages the physiscs world
	*/
	class World
	{
	private:

		// Keep track of the shapes, states and rigid bodies.
		// Make sure to reuse collision shapes among rigid bodies whenever possible!

		vector< shared_ptr< btRigidBody          > > rigidBodies;
		vector< shared_ptr< btDefaultMotionState > > motionStates;
		vector< shared_ptr< btCollisionShape     > > collisionShapes;


		// Collision configuration contains default setup for memory, collision setup.
		// Advanced users can create their own configuration.

		btDefaultCollisionConfiguration collisionConfiguration;

		// Use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded).

		btCollisionDispatcher * collisionDispatcher;

		// btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.

		btDbvtBroadphase overlappingPairCache;

		// The default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded).

		btSequentialImpulseConstraintSolver constraintSolver;

		// Create and configure the physiscs world:

		btDiscreteDynamicsWorld * dynamicsWorld;

	public:

		World() { init(); }

		~World() {}

		void push_back(shared_ptr< btRigidBody > body)
		{
			rigidBodies.push_back(body);
		}

		void push_back(shared_ptr< btCollisionShape > shape)
		{
			collisionShapes.push_back(shape);
		}

		void push_back(shared_ptr< btDefaultMotionState > motion)
		{
			motionStates.push_back(motion);
		}

		btDiscreteDynamicsWorld * get_dynamicsWorld()
		{
			return dynamicsWorld;
		}

		void init();

		// Call to change grav
		void set_gravity(btVector3 &grav)
		{
			dynamicsWorld->setGravity(grav);
		}


	};
}
#endif // !WORLD_HEADER
