var hierarchy =
[
    [ "b2ContactListener", null, [
      [ "listener::MyContactListener", "classlistener_1_1_my_contact_listener.html", null ]
    ] ],
    [ "entities::Entity", "classentities_1_1_entity.html", [
      [ "entities::Ball", "classentities_1_1_ball.html", null ],
      [ "entities::Basket", "classentities_1_1_basket.html", null ],
      [ "entities::Car", "classentities_1_1_car.html", null ],
      [ "entities::Elevator", "classentities_1_1_elevator.html", null ],
      [ "entities::Ground", "classentities_1_1_ground.html", null ],
      [ "entities::Platform", "classentities_1_1_platform.html", null ],
      [ "entities::SensorEntity", "classentities_1_1_sensor_entity.html", null ]
    ] ],
    [ "scene::Scene", "classscene_1_1_scene.html", null ]
];