#include "MyContactListener.hpp"
using namespace listener;

bool get_entities(b2Contact * contact, b2Body*& sensor, b2Body*& other)
{
	b2Fixture* fixture_A = contact->GetFixtureA();
	b2Fixture* fixture_B = contact->GetFixtureB();

	//check which one of them is a sensor
	bool sensor_A = fixture_A->IsSensor();
	bool sensor_B = fixture_B->IsSensor();
	if (!(sensor_A ^ sensor_B))
		return false;

	b2Body * entity_A = fixture_A->GetBody();
	b2Body * entity_B = fixture_B->GetBody();

	if (sensor_A)
	{
		sensor = entity_A;
		other = entity_B;
	}

	if (sensor_B)
	{
		sensor = entity_B;
		other = entity_A;
	}
	return true;
}

void MyContactListener::BeginContact(b2Contact * contact)
{
	b2Body * sensor;
	b2Body * other;
	vector <SensorEntity> sensors = _scene->get_sensors();

	if (get_entities(contact, sensor, other))
	{
		for (unsigned int count = 0; count < sensors.size(); count++)
		{
			if (sensors[count].get_body() == sensor)
			{
				if (sensors[count].get_name() == "sensor_basket" && other == _scene->get_car()->get_body())
				{
					_scene->activate_basket();
					_scene->get_elevator()->activate();
				}
				else if (sensors[count].get_name() == "sensor_elevator" && other == _scene->get_car()->get_body())
					_scene->get_elevator()->elevate();
			}	
		}

	}
	
}

MyContactListener::MyContactListener()
{}

void MyContactListener::set_scene(Scene * scene)
{
	_scene = scene;
}

MyContactListener::~MyContactListener()
{
}

