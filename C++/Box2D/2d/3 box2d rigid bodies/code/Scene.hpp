/**
* Author: Jiale Cheng
*/

#ifndef SCENE_HEADER
#define SCENE_HEADER
#include "SensorEntity.hpp"
#include "Car.hpp"
#include "Ball.hpp"
#include "Ground.hpp"
#include "Basket.hpp"
#include "Platform.hpp"
#include "Ball.hpp"
#include "Elevator.hpp"

using namespace sf;
using namespace std;
using namespace entities;

namespace scene
{
	/**
	* @class Scene
	* @brief class that contains all the entities and manages them
	*/
	class Scene
	{

	private:
		shared_ptr<b2World> scene_world;

		SensorEntity sensor_basket;
		SensorEntity sensor_elevator;
		SensorEntity sensor_final;
		vector<SensorEntity> sensors;

		Car _car;
		Ball balls[10];
		Ground _ground;
		Basket _basket;
		Platform _platform;
		Elevator _elevator;
		vector<Entity *> objects;

		float physics_height;
		float physics_width;
		float physics_graphics_scale;

	public:
		Scene(float scale, float width, float height);
		~Scene();

		Car * get_car() { return &_car; }

		Elevator * get_elevator() { return &_elevator; }

		void create_balls(shared_ptr< b2World > physics_world);

		void render(RenderWindow & window);

		void update();

		void create_physics_world();

		shared_ptr< b2World > get_physics_world() { return scene_world; }

		void activate_basket();

		float get_graphic_scale() { return physics_graphics_scale; }

		void add_sensor(SensorEntity sensor) { sensors.push_back(sensor); }

		void add_object(Entity *object) { objects.push_back(object); }

		vector<SensorEntity> get_sensors() { return sensors; }

		vector<Entity *> get_objects() { return objects; }


	};
}

#endif