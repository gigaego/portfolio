/**
* Author: Jiale Cheng
*/

#ifndef ENTITY_HEADER
#define ENTITY_HEADER

#include <SFML/Window.hpp>
#include <Box2D/Box2D.h>
#include <memory>
#include <vector>
#include <SFML/Graphics.hpp>
#include <list>


using namespace sf;
using namespace std;

namespace entities
{
	/**
	* @class Entity
	* @brief Base class of all entities
	*/
	class Entity
	{
	private:

		b2Body * body;
		shared_ptr <Shape> shape;
		vector<Entity *> children;
		string _name;

	public:
		Entity();

		~Entity();

		/** En Box2D las coordenadas Y crecen hacia arriba y en SFML crecen hacia abajo desde el borde superior.
		** Esta funci�n se encarga de convertir el sistema de coordenadas para que la escena no se vea invertida.
		**/
		inline Vector2f box2d_position_to_sfml_position(const b2Vec2 & box2d_position, float window_height)
		{
			return Vector2f(box2d_position.x, window_height - box2d_position.y);
		}


		b2Body * get_body() { return body; }

		vector<Entity *> get_children()
		{
			return children;
		}

		void add_child(Entity *child)
		{
			children.push_back(child);
		}

		virtual string get_name();

		void set_name(string name) { _name = name; }

		//creates the type of the body
		void create_dynamic_body(shared_ptr<b2World> world, float pos_x, float pos_y);
		void create_kinematic_body(shared_ptr<b2World> world, float pos_x, float pos_y);
		void create_static_body(shared_ptr<b2World> world, float pos_x, float pos_y);

		//Add sensor
		void add_sensor(float width, float length, float angle, float density, float restitution, float friction);

		//Add fixtures
		void add_circle_fixture(float radius, b2Vec2 relative_pos, float density, float restitution, float friction);
		void add_box_fixture(float width, float length, b2Vec2 relative_pos, float angle, float density, float restitution, float friction);
		void add_polygon_fixture(b2Vec2 vertex[], int vertex_lenght, float angle, float density, float restitution, float friction);

		//Render with a specific color or not
		virtual void render(RenderWindow & window);
		virtual void render(RenderWindow & window, Color col);

		virtual void update() { return; }

	};
}

#endif