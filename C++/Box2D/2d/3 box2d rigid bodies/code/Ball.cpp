#include "Ball.hpp"
using namespace entities;

void Ball::render(RenderWindow & window)
{
	b2Body * body = get_body();
	const b2Transform & body_transform = body->GetTransform();
	float window_height = (float)window.getSize().y;
	for (b2Fixture * fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{

		// Se crea un CircleShape a partir de los atributos de la forma de la fixture y del body:
		// En SFML el centro de un c�rculo no est� en su position. Su position es la esquina superior izquierda
		// del cuadrado en el que est� inscrito. Por eso a position se le resta el radio tanto en X como en Y.

		b2CircleShape * circle = dynamic_cast<b2CircleShape *>(fixture->GetShape());

		float  radius = circle->m_radius;
		b2Vec2 center = circle->m_p;

		CircleShape shape;

		shape.setPosition(box2d_position_to_sfml_position(b2Mul(body_transform, center), window_height) - Vector2f(radius, radius));
		shape.setFillColor(Color::Yellow);
		shape.setRadius(radius);

		window.draw(shape);
			
	}
}

void Ball::init(shared_ptr<b2World> world, b2Vec2 pos, float radius, b2Vec2 relative_pos, float density, float restitution, float friction)
{
	create_dynamic_body(world, pos.x, pos.y);
	add_circle_fixture(radius, relative_pos, density, restitution, friction);
	set_name("ball");
}

Ball::Ball()
{

}

Ball::~Ball()
{
}
