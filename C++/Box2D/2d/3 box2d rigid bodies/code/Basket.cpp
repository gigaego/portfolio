#include "Basket.hpp"
using namespace entities;

void Basket::init(shared_ptr<b2World> physics_world)
{
	create_static_body(physics_world, 600, 300);
	//basket.set_name("basket");
	basket.create_dynamic_body(physics_world, 600, 300);
	//basket.add_circle_fixture(10.f, b2Vec2(0.f, 0.f), 1.f, 0.75, 0.5);
	b2Vec2 vertex[4];
	vertex[0].Set(0, 10);
	vertex[1].Set(0, -20);
	vertex[2].Set(80, 60);
	vertex[3].Set(80, 90);
	basket.add_polygon_fixture(vertex, 4, 0, 1, 0.75, 0.5);
	vertex[0].Set(0, 10);
	vertex[1].Set(0, -20);
	vertex[2].Set(-80, 60);
	vertex[3].Set(-80, 90);
	basket.add_polygon_fixture(vertex, 4, 0, 1, 0.75, 0.5);
	add_child(&basket);

	b2RevoluteJointDef joint_def;
	joint_def.bodyA = basket.get_body();
	joint_def.bodyB = get_body();
	joint_def.collideConnected = true;
	joint_def.localAnchorA.Set(0, 0);
	joint_def.localAnchorB.Set(0, 0);
	joint_def.enableLimit = true;
	joint_def.lowerAngle = 0.0;
	joint_def.upperAngle = 0.0;

	joint_def.enableMotor = false;
	joint_def.maxMotorTorque = 2000000.f;
	joint_def.motorSpeed = 800.f;

	basket_joint = (b2RevoluteJoint*)physics_world->CreateJoint(&joint_def);

	pillar.setSize(Vector2f(20, 400));
	pillar.setFillColor(Color::White);
	pillar.setPosition(590, 300);
	
	set_name("basket");
}

void Basket::render(RenderWindow & window)
{
	b2Body * body = get_body();
	const b2Transform & body_transform = body->GetTransform();
	float window_height = (float)window.getSize().y;
	vector< Entity *> children = get_children();
	window.draw(pillar);

	for (unsigned int i = 0; i < children.size(); ++i)
	{
		children[i]->render(window);
	}

	
}

Basket::Basket() {}

Basket::~Basket()
{
}