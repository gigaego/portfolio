/**
* Author: Jiale Cheng
*/
#ifndef ELEVATOR_HEADER
#define ELEVATOR_HEADER

#include "Entity.hpp"

namespace entities
{
	/**
	* @class Elevator
	* @brief Platform that goes up when it is activated
	*/
	class Elevator : public Entity
	{
	public:
		Elevator();
		void init(shared_ptr<b2World> physics_world);
		void activate();
		void elevate();
		void update() override;
		void render(RenderWindow & window);
		~Elevator();

	private:
		bool first_trigger;
		bool second_trigger;
	};
}

#endif // !ELEVATOR_HEADER