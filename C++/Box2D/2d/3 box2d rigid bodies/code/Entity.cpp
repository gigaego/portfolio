#include "Entity.hpp"
using namespace entities;

void Entity::create_dynamic_body(shared_ptr<b2World> world, float pos_x, float pos_y)
{
	b2BodyDef body_def;
	body_def.type = b2_dynamicBody;
	body_def.position.Set(pos_x, pos_y);

	body = world->CreateBody(&body_def);
	get_body()->SetUserData(this);
}

void Entity::create_kinematic_body(shared_ptr<b2World> world, float pos_x, float pos_y)
{
	b2BodyDef body_def;
	body_def.type = b2_kinematicBody;
	body_def.position.Set(pos_x, pos_y);

	body = world->CreateBody(&body_def);
	get_body()->SetUserData(this);
}

void Entity::create_static_body(shared_ptr<b2World> world, float pos_x, float pos_y)
{
	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	body_def.position.Set(pos_x, pos_y);

	body = world->CreateBody(&body_def);
	get_body()->SetUserData(this);
}

/**
* @brief Add a sensor with a box shape
*/
void Entity::add_sensor(float width, float length, float angle, float density, float restitution, float friction)
{
	b2PolygonShape shape;
	shape.SetAsBox(width, length, b2Vec2(0, 0), angle);

	b2FixtureDef body_fixture;
	body_fixture.isSensor = true;
	body_fixture.shape = &shape;
	body_fixture.density = density;
	body_fixture.restitution = restitution;
	body_fixture.friction = friction;

	body->CreateFixture(&body_fixture);
}

void Entity::add_circle_fixture(float radius, b2Vec2 relative_pos, float density, float restitution, float friction)
{
	b2CircleShape shape;
	shape.m_p = relative_pos;
	shape.m_radius = radius;

	b2FixtureDef body_fixture;
	body_fixture.shape = &shape;
	body_fixture.density = density;
	body_fixture.restitution = restitution;
	body_fixture.friction = friction;

	body->CreateFixture(&body_fixture);
}

void Entity::add_box_fixture(float width, float length, b2Vec2 relative_pos, float angle, float density, float restitution, float friction)
{
	b2PolygonShape shape;
	shape.SetAsBox(width, length, relative_pos, angle);

	b2FixtureDef body_fixture;
	body_fixture.shape = &shape;
	body_fixture.density = density;
	body_fixture.restitution = restitution;
	body_fixture.friction = friction;

	body->CreateFixture(&body_fixture);
}

void Entity::add_polygon_fixture(b2Vec2 vertex[], int vertex_lenght, float angle, float density, float restitution, float friction)
{
	b2PolygonShape shape;
	shape.Set(vertex, vertex_lenght);

	b2FixtureDef body_fixture;
	body_fixture.shape = &shape;
	body_fixture.density = density;
	body_fixture.restitution = restitution;
	body_fixture.friction = friction;

	body->CreateFixture(&body_fixture);
}

void Entity::render(RenderWindow & window, Color col)
{
	b2Body * body = get_body();
	const b2Transform & body_transform = body->GetTransform();
	float window_height = (float)window.getSize().y;
	for (b2Fixture * fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		// Se obtiene el tipo de forma de la fixture:
		b2Shape::Type shape_type;
		if (fixture->IsSensor() == false)
		{
			shape_type = fixture->GetShape()->GetType();

			if (shape_type == b2Shape::e_circle)
			{
				// Se crea un CircleShape a partir de los atributos de la forma de la fixture y del body:
				// En SFML el centro de un c�rculo no est� en su position. Su position es la esquina superior izquierda
				// del cuadrado en el que est� inscrito. Por eso a position se le resta el radio tanto en X como en Y.

				b2CircleShape * circle = dynamic_cast< b2CircleShape * >(fixture->GetShape());

				float  radius = circle->m_radius;
				b2Vec2 center = circle->m_p;

				CircleShape shape;

				shape.setPosition(box2d_position_to_sfml_position(b2Mul(body_transform, center), window_height) - Vector2f(radius, radius));
				shape.setFillColor(col);
				shape.setRadius(radius);

				window.draw(shape);
			}
			else if (shape_type == b2Shape::e_polygon)
			{
				// Se toma la forma poligonal de Box2D (siempre es convexa) y se crea a partir de sus v�rtices un
				// ConvexShape de SFML. Cada v�rtice de Box2D hay que transformarlo usando el transform del body.

				b2PolygonShape * box2d_polygon = dynamic_cast< b2PolygonShape * >(fixture->GetShape());
				ConvexShape       sfml_polygon;

				int number_of_vertices = box2d_polygon->GetVertexCount();

				sfml_polygon.setPointCount(number_of_vertices);
				sfml_polygon.setFillColor(col);

				for (int index = 0; index < number_of_vertices; index++)
				{
					sfml_polygon.setPoint
					(
						index,
						box2d_position_to_sfml_position(b2Mul(body_transform, box2d_polygon->GetVertex(index)), window_height)
					);
				}

				window.draw(sfml_polygon);
			}
		}

	}
}

void Entity::render(RenderWindow & window)
{
	b2Body * body = get_body();
	const b2Transform & body_transform = body->GetTransform();
	float window_height = (float)window.getSize().y;
	for (b2Fixture * fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		// Se obtiene el tipo de forma de la fixture:
		b2Shape::Type shape_type;
		if (fixture->IsSensor() == false)
		{
			shape_type = fixture->GetShape()->GetType();

			if (shape_type == b2Shape::e_circle)
			{
				// Se crea un CircleShape a partir de los atributos de la forma de la fixture y del body:
				// En SFML el centro de un c�rculo no est� en su position. Su position es la esquina superior izquierda
				// del cuadrado en el que est� inscrito. Por eso a position se le resta el radio tanto en X como en Y.

				b2CircleShape * circle = dynamic_cast< b2CircleShape * >(fixture->GetShape());

				float  radius = circle->m_radius;
				b2Vec2 center = circle->m_p;

				CircleShape shape;

				shape.setPosition(box2d_position_to_sfml_position(b2Mul(body_transform, center), window_height) - Vector2f(radius, radius));
				shape.setFillColor(Color::Blue);
				shape.setRadius(radius);

				window.draw(shape);
			}
			else if (shape_type == b2Shape::e_polygon)
			{
				// Se toma la forma poligonal de Box2D (siempre es convexa) y se crea a partir de sus v�rtices un
				// ConvexShape de SFML. Cada v�rtice de Box2D hay que transformarlo usando el transform del body.

				b2PolygonShape * box2d_polygon = dynamic_cast< b2PolygonShape * >(fixture->GetShape());
				ConvexShape       sfml_polygon;

				int number_of_vertices = box2d_polygon->GetVertexCount();

				sfml_polygon.setPointCount(number_of_vertices);
				sfml_polygon.setFillColor(Color::Cyan);

				for (int index = 0; index < number_of_vertices; index++)
				{
					sfml_polygon.setPoint
					(
						index,
						box2d_position_to_sfml_position(b2Mul(body_transform, box2d_polygon->GetVertex(index)), window_height)
					);
				}

				window.draw(sfml_polygon);
			}
		}

	}
}

string Entity::get_name()
{
	return _name;
}

Entity::Entity()
{
	_name = "default";
	body  = nullptr;
}

Entity::~Entity()
{
}