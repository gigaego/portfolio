#include "SensorEntity.hpp"
using namespace entities;

void SensorEntity::init(shared_ptr<b2World> world, float pos_x, float pos_y, float width, float lenght, float angle, float density, float restitution, float friction)
{
	create_static_body(world, pos_x, pos_y);
	add_sensor(width, lenght, angle, density, restitution, friction);

}

SensorEntity::SensorEntity(){}

SensorEntity::~SensorEntity()
{

}
