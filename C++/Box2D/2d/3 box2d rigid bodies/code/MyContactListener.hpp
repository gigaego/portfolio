/**
* Author: Jiale Cheng
*/

#ifndef MYCONTACTLISTENER_HEADER
#define MYCONTACTLISTENER_HEADER
#include <Box2D/Box2D.h>
#include "Scene.hpp"

using namespace scene;

namespace listener
{
	/**
	* @class MyContactListener
	* @brief A class that inheritance from Box2D's b2ContactListener and checks collisions
	*/
	class MyContactListener : public b2ContactListener
	{
	private:
		Scene * _scene;
	public:
		MyContactListener();
		MyContactListener(Scene * scene) : _scene(scene) {}
		~MyContactListener();
		void set_scene(Scene * scene);
		Scene get_scene() { return *_scene; }
		void BeginContact(b2Contact * contact);
	};
}

#endif