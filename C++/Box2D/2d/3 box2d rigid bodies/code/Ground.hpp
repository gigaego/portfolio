/**
* Author: Jiale Cheng
*/
#ifndef GROUND_HEADER
#define GROUND_HEADER
#include "Entity.hpp"

namespace entities
{
	/**
	* @class Ground
	* @brief Entity that is the ground.
	*/
	class Ground :public Entity
	{
	public:
		void init(shared_ptr<b2World> physics_world, float physics_width);
		void render(RenderWindow & window);
		Ground();
		~Ground();

	private:
		Entity floor_left;
		Entity floor_right;
		Entity floor_right_2;
	};
}

#endif