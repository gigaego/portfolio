#include "Car.hpp"
using namespace entities;

Car::Car() 
{
	
}

void Car::update()
{
	if (get_body()->GetPosition().y <= 45.f)
	{
		reset();
	}
}

void Car::reset() 
{
	for (auto & child : get_children())
	{
		child->get_body()->SetTransform(b2Vec2(25, 120), 0.f);
	}
	get_body()->SetTransform(b2Vec2(25, 120), 0.f);

	stop();
}

void Car::init(shared_ptr<b2World> physics_world)
{
	set_name("car");
	create_dynamic_body(physics_world, 25, 150);
	add_box_fixture(40.f, 10.f, b2Vec2(0, 0), 0.0f, 10.0f, 0.f, 0.5f);

	left_wheel.create_dynamic_body(physics_world, 0, 150);
	left_wheel.add_circle_fixture(20, b2Vec2(40.f, 40.f), 10.0f, 0.75f, 1.f);
	add_child(&left_wheel);
	left_wheel.set_name("car");

	//(physics_world, b2Vec2(0, 150), 20.f, b2Vec2(40.f, 40.f), 1.00f, 0.75f, 0.f);
	right_wheel.create_dynamic_body(physics_world, 0, 150);
	right_wheel.add_circle_fixture(20, b2Vec2(40.f, 40.f), 10.0f, 0.75f, 0);
	add_child(&right_wheel);
	right_wheel.set_name("car");

	//JOINT TEST
	b2RevoluteJointDef joint_def;
	joint_def.bodyA = left_wheel.get_body();
	joint_def.bodyB = get_body();
	joint_def.collideConnected = false;
	joint_def.localAnchorA = left_wheel.get_body()->GetLocalCenter();
	joint_def.localAnchorB.Set(-25, -10);
	joint_def.upperAngle = 0.01f;
	joint_def.lowerAngle = 0.0f;

	joint_def.enableMotor = true;
	joint_def.maxMotorTorque = 26500000.f;
	joint_def.motorSpeed = 1000.f;

	car_wheel_joint = (b2RevoluteJoint*)physics_world->CreateJoint(&joint_def);

	b2RevoluteJointDef joint_def2;
	joint_def2.bodyA = right_wheel.get_body();
	joint_def2.bodyB = get_body();
	joint_def2.collideConnected = false;
	joint_def2.localAnchorA = right_wheel.get_body()->GetLocalCenter();
	joint_def2.localAnchorB.Set(25, -10);

	joint_def2.enableMotor = true;
	joint_def2.maxMotorTorque = 15000000.f;
	joint_def2.motorSpeed = -200.f;

	b2RevoluteJoint * wheel_joint = (b2RevoluteJoint*)physics_world->CreateJoint(&joint_def2);

	//joint->EnableMotor(false);

	car_basket.create_dynamic_body(physics_world, 0, 180);
	car_basket.add_box_fixture(40, 5, b2Vec2(5, 7.5f), 0, 4.f, 0.75, 0.5);

	car_basket.add_box_fixture(5, 30, b2Vec2(-30.f , 35.f), 0, 0.5f, 0.75, 0.5);

	car_basket.add_box_fixture(5, 30, b2Vec2(40.f, 35.f), 0, 0.5f, 0.75, 0.5);
	add_child(&car_basket);

	b2RevoluteJointDef joint_def3;
	joint_def3.bodyA = car_basket.get_body();
	joint_def3.bodyB = get_body();
	joint_def3.collideConnected = false;
	joint_def3.localAnchorA.Set(-25, -20);
	joint_def3.localAnchorB.Set(-25, 0);
	joint_def3.enableLimit = true;
	joint_def3.upperAngle = 0.01;
	joint_def3.lowerAngle = -0.01;

	joint_def3.enableMotor = false;
	joint_def3.maxMotorTorque = 50000000.f;
	joint_def3.motorSpeed = -800.f;

	car_basket_joint = (b2RevoluteJoint*)physics_world->CreateJoint(&joint_def3);
	stop();
}

void Car::right_acccel() 
{

	get_body()->SetType(b2_dynamicBody);
	car_wheel_joint->EnableLimit(false);
	if (car_wheel_joint->GetMotorSpeed() <= 0)
	{
		car_wheel_joint->SetMotorSpeed(1000.f);
	}
	car_wheel_joint->EnableMotor(true);
	
	//get_body()->SetLinearVelocity(b2Vec2(100, -100));

}

void Car::left_accel() 
{
	get_body()->SetType(b2_dynamicBody);
	car_wheel_joint->EnableLimit(false);
	if (car_wheel_joint->GetMotorSpeed() >= 0)
	{
		car_wheel_joint->SetMotorSpeed(-1000.f);
	}
	car_wheel_joint->EnableMotor(true);
}

void Car::stop()
{
	car_wheel_joint->EnableMotor(false);
	get_body()->SetType(b2_kinematicBody);
	get_body()->SetLinearVelocity(b2Vec2(0, -10));
	get_body()->SetType(b2_dynamicBody);
}

void Car::basket_active()
{
	car_basket_joint->SetLimits(-1.8, 0.f);
	car_basket_joint->EnableMotor(true);
}

void Car::render(RenderWindow & window)
{
	b2Body * body = get_body();
	const b2Transform & body_transform = body->GetTransform();
	float window_height = (float)window.getSize().y;
	vector< Entity *> children = get_children();

	b2PolygonShape * box2d_polygon = dynamic_cast< b2PolygonShape * >(get_body()->GetFixtureList()->GetShape());
	ConvexShape       sfml_polygon;

	int number_of_vertices = box2d_polygon->GetVertexCount();

	sfml_polygon.setPointCount(number_of_vertices);
	sfml_polygon.setFillColor(Color::Blue);

	for (int index = 0; index < number_of_vertices; index++)
	{
		sfml_polygon.setPoint
		(
			index,box2d_position_to_sfml_position(b2Mul(body_transform, box2d_polygon->GetVertex(index)), window_height)
		);
	}

	window.draw(sfml_polygon);

	for (unsigned int i = 0; i < children.size(); ++i)
	{
		if(i!=2)
			children[i]->render(window, Color::White);
		else
			children[i]->render(window, Color::Magenta);
	}

}

Car::~Car()
{
}