#include "Scene.hpp"
using namespace scene;

/** En Box2D las coordenadas Y crecen hacia arriba y en SFML crecen hacia abajo desde el borde superior.
** Esta funci�n se encarga de convertir el sistema de coordenadas para que la escena no se vea invertida.
**/

inline Vector2f box2d_position_to_sfml_position(const b2Vec2 & box2d_position, float window_height)
{
	return Vector2f(box2d_position.x, window_height - box2d_position.y);
}


void Scene::render(RenderWindow & window)
{
	vector<Entity * > aux_list = _car.get_children();
	for(unsigned int i = 0; i < objects.size(); i++)
	{
		objects[i]->render(window);
	}
	
}

void Scene::create_physics_world()
{
	// Se crea el mundo f�sico:
	
	shared_ptr< b2World > physics_world(new b2World(b2Vec2(0, -100.f)));

	// Initialize all entities
	_platform.init(physics_world);
	_ground.init(physics_world, physics_width);
	_car.init(physics_world);
	_basket.init(physics_world);
	_elevator.init(physics_world);
	create_balls(physics_world);

	// Push the entities to a vector
	add_object(&_platform);
	add_object(&_ground);
	add_object(&_car);
	add_object(&_basket);
	add_object(&_elevator);

	// Initialize the sensors
	sensor_basket.init(physics_world, 750.f, 120.f, 100.f, 40.f, 0.1f, 1.0, 0.75, 0);
	sensor_basket.set_name("sensor_basket");

	sensor_elevator.init(physics_world, 440.f, 50.f, 25.f, 10.f, 0, 1.0, 0.75, 0);
	sensor_elevator.set_name("sensor_elevator");

	sensor_final.init(physics_world, 95.f, 340.f, 25.f, 10.f, 0, 1.0, 0.75, 0);
	sensor_final.set_name("sensor_final");

	add_sensor(sensor_basket);
	add_sensor(sensor_elevator);
	add_sensor(sensor_final);
	
	scene_world = physics_world;
}

void Scene::activate_basket()
{
	_basket.get_joint()->SetLimits(-1.57, 1.57); //~90�
	_basket.get_joint()->EnableMotor(true);
}

void Scene::create_balls(shared_ptr< b2World > physics_world)
{
	for (size_t i = 0; i < 10; i++)
	{
		balls[i].init(physics_world, b2Vec2(600, 340 + 20*i), 10, b2Vec2(0, 0), 0.1, 0.5, 0.5);
		add_object(&balls[i]);
	}
}

Scene::Scene(float scale, float width, float height)
{
	physics_height = height;
	physics_width = width;
	physics_graphics_scale = scale;
}

void Scene::update() 
{
	for (auto & object : objects)
	{
		object->update();
	}
}

Scene::~Scene()
{

}