#include "Platform.hpp"
using namespace entities;

void Platform::render(RenderWindow & window)
{
	b2Body * body = get_body();
	const b2Transform & body_transform = body->GetTransform();
	float window_height = (float)window.getSize().y;
	vector< Entity *> children = get_children();

	for (unsigned int i = 0; i < children.size(); ++i)
	{
		children[i]->render(window);
	}

}

void Platform::init(shared_ptr<b2World> physics_world)
{
	create_static_body(physics_world, 240, 350);
	
	platform.create_static_body(physics_world, 240, 350);
	platform.add_box_fixture(100, 10, b2Vec2(35, 0), 0, 1, 0.75, 0.5);
	//platform.add_circle_fixture(10, b2Vec2(-150, -50), 1, 0.75, 0.5);
	b2Vec2 vertex[4];
	vertex[0].Set(-150, -40);
	vertex[1].Set(-65, 10);
	vertex[2].Set(-65, -10);
	vertex[3].Set(-150, -60);
	platform.add_polygon_fixture(vertex, 4, 0, 1, 0.75, 0.5);

	vertex[0].Set(-150, -40);
	vertex[1].Set(-215, 10);
	vertex[2].Set(-215, -10);
	vertex[3].Set(-150, -60);
	platform.add_polygon_fixture(vertex, 4, 0, 1, 0.75, 0.5);

	platform.set_name("platform");
	add_child(&platform);
}

Platform::Platform(){}

Platform::~Platform()
{
}
