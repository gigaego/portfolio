/**
* Author: Jiale Cheng
*/

#ifndef PLATFORM_HEADER
#define PLATFORM_HEADER
#include "Entity.hpp"

namespace entities
{
	/**
	* @class Platform
	* @brief Class of the platform
	*/
	class Platform :public Entity
	{
	public:
		void init(shared_ptr<b2World> physics_world);
		void render(RenderWindow & window);
		Platform();
		~Platform();
	private:
		Entity platform;
	};
}

#endif