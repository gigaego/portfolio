/**
* Author: Jiale Cheng
*/

#ifndef BASKET_HEADER
#define BASKET_HEADER
#include "Entity.hpp"
#include "Ball.hpp"

namespace entities
{
	/**
	* @class Basket
	* @brief Entity which purpose is to contain the balls and overturn them 
	*/
	class Basket :public Entity
	{
	public:
		void init(shared_ptr<b2World> physics_world);
		Basket();
		//reference to the joint that overturns the basket
		b2RevoluteJoint * get_joint() { return basket_joint; }
		void render(RenderWindow & window);
		~Basket();

	private:
		Entity basket;
		//pilar del basket
		RectangleShape pillar;
		b2RevoluteJoint * basket_joint;
	};
}
#endif
