#include "Ground.hpp"
using namespace entities;

void Ground::render(RenderWindow & window)
{
	b2Body * body = get_body();
	const b2Transform & body_transform = body->GetTransform();
	float window_height = (float)window.getSize().y;
	vector< Entity *> children = get_children();

	
	for (unsigned int i = 0; i < children.size(); ++i)
	{
		if (i == 1) 
		{
			children[1]->render(window, Color::Yellow);
		}
		else
		children[i]->render(window);
	}

}

void Ground::init(shared_ptr<b2World> physics_world, float physics_width)
{
	b2Vec2 vertex[4];
	create_static_body(physics_world, 35.f, 40.f);
	
	floor_left.create_static_body(physics_world, 35.f, 40.f);
	floor_left.add_box_fixture(70.f, 80.f, b2Vec2(0, 0), 0.0f, 5.00f, 0.f, 0.5f);
	vertex[0].Set(70, 80);
	vertex[1].Set(70, -80);
	vertex[2].Set(120, -80);
	vertex[3].Set(120, 40);
	floor_left.add_polygon_fixture(vertex, 4, 0.0f, 5.00f, 0.f, 0.5f);

	vertex[0].Set(120, 40);
	vertex[1].Set(120, -50);
	vertex[2].Set(145, -50);
	vertex[3].Set(145, 30);
	floor_left.add_polygon_fixture(vertex, 4, 0.0f, 5.00f, 0.f, 0.5f);

	vertex[0].Set(145, 30);
	vertex[1].Set(145, -50);
	vertex[2].Set(170, -50);
	vertex[3].Set(170, 20);
	floor_left.add_polygon_fixture(vertex, 4, 0.0f, 5.00f, 0.f, 0.5f);

	vertex[0].Set(170, 20);
	vertex[1].Set(170, -50);
	vertex[2].Set(220, -50);
	vertex[3].Set(220, 10);
	floor_left.add_polygon_fixture(vertex, 4, 0.0f, 5.00f, 0.f, 0.5f);

	vertex[0].Set(220, 10);
	vertex[1].Set(220, -50);
	vertex[2].Set(270, -50);
	vertex[3].Set(270, 5);
	floor_left.add_polygon_fixture(vertex, 4, 0.0f, 5.00f, 0.f, 0.5f);

	vertex[0].Set(270, 5);
	vertex[1].Set(270, -50);
	vertex[2].Set(320, -50);
	vertex[3].Set(320, 15);
	floor_left.add_polygon_fixture(vertex, 4, 0.0f, 5.00f, 0.f, 0.5f);

	vertex[0].Set(320, 15);
	vertex[1].Set(320, -50);
	vertex[2].Set(340, -50);
	vertex[3].Set(340, 25);
	floor_left.add_polygon_fixture(vertex, 4, 0.0f, 5.00f, 0.f, 0.5f);

	add_child(&floor_left);

	floor_right.create_static_body(physics_world, physics_width - 20.f, 0.f);
	vertex[0].Set(-50, 60);
	vertex[1].Set(-50, -60);
	vertex[2].Set(50, -60);
	vertex[3].Set(50, 70);
	floor_right.add_polygon_fixture(vertex, 4, 0.0f, 5.00f, 0.f, 0.5f);

	add_child(&floor_right);

	floor_right_2.create_static_body(physics_world, physics_width - 170.f, 0.f);
	vertex[0].Set(-125, 40);
	vertex[1].Set(-125, -40);
	vertex[2].Set(100, -40);
	vertex[3].Set(100, 60);
	floor_right_2.add_polygon_fixture(vertex, 4, 0.0f, 5.00f, 0.f, 0.5f);

	add_child(&floor_right_2);

	set_name("ground");
}

Ground::Ground() {}

Ground::~Ground()
{
}
