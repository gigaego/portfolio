
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                             *
 *  Started by Ángel on april of 2016                                          *
 *                                                                             *
 *  This is free software released into the public domain.                     *
 *                                                                             *
 *  angel.rodriguez@esne.edu												   *
 *																			   *
 *  Modified by Jiale Cheng													   *
 *                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "Scene.hpp"
#include "MyContactListener.hpp"
#include <Windows.h>

using namespace sf;
using namespace std;
using namespace scene;
using namespace listener;

int main ()
{
    RenderWindow window(VideoMode(800, 600), "Animation Examples: Box2D Rigid Bodies", Style::Titlebar | Style::Close, ContextSettings(32));

    window.setVerticalSyncEnabled (true);

	Scene scene(1.f, 800, 600);
	scene.create_physics_world();
	MyContactListener contact_listener;
	contact_listener.set_scene(&scene);
    shared_ptr< b2World > physics_world = scene.get_physics_world();
	physics_world->SetContactListener(&contact_listener);
    bool running = true;

    Clock timer;
    float delta_time = 0.017f;          // ~60 fps
	int c = 0;
    do
    {
        timer.restart ();

        // Process window events:

        Event event;

        while (window.pollEvent (event))
        {
            if (event.type == Event::Closed)
            {
                running = false;
            }
        }

        // Update:

        physics_world->Step (delta_time, 8, 4);

		scene.update();

		if (GetAsyncKeyState(VK_UP) & 0x8000)
		{
			scene.get_car()->basket_active();
		}

		if (GetAsyncKeyState(VK_DOWN) & 0x8000)
		{
			scene.get_car()->stop();
		}

		if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
		{
			scene.get_car()->right_acccel();
		}

		if (GetAsyncKeyState(VK_LEFT) & 0x8000)
		{
			scene.get_car()->left_accel();
		}

        // Render:

        window.clear ();

        scene.render (window);

        window.display ();

        delta_time = (delta_time + timer.getElapsedTime ().asSeconds ()) * 0.5f;
    }
    while (running);

    return EXIT_SUCCESS;
}
