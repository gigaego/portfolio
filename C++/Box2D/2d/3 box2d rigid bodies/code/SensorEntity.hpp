/**
* Author: Jiale Cheng
*/

#ifndef SENSORENTITY_HEADER
#define SENSORENTITY_HEADER
#include "Entity.hpp"

namespace entities
{
	/**
	* @class SensorEntity
	* @brief Entity that it's not rendered and just acts as a trigger
	*/
	class SensorEntity :public Entity
	{
	public:
		void init(std::shared_ptr<b2World> world, float pos_x, float pos_y, float width, float lenght, float angle, float density, float restitution, float friction);
		SensorEntity();
		~SensorEntity();

	private:
		Entity * _entity;
	};
}
#endif