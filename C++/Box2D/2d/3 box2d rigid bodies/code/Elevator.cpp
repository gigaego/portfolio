#include "Elevator.hpp"
using namespace entities;

void Elevator::render(RenderWindow & window)
{
	b2Body * body = get_body();
	const b2Transform & body_transform = body->GetTransform();
	float window_height = (float)window.getSize().y;

	b2PolygonShape * box2d_polygon = dynamic_cast<b2PolygonShape *>(get_body()->GetFixtureList()->GetShape());
	ConvexShape       sfml_polygon;

	int number_of_vertices = box2d_polygon->GetVertexCount();

	sfml_polygon.setPointCount(number_of_vertices);
	sfml_polygon.setFillColor(Color::Red);

	for (int index = 0; index < number_of_vertices; index++)
	{
		sfml_polygon.setPoint
		(
			index,
			box2d_position_to_sfml_position(b2Mul(body_transform, box2d_polygon->GetVertex(index)), window_height)
		);
	}

	window.draw(sfml_polygon);
}

Elevator::Elevator()
{
}

void Elevator::update()
{
	b2Body *body = get_body();
	if (body->GetPosition().y < 24.f && first_trigger == true) 
	{
		body->SetLinearVelocity(b2Vec2(0, 50));
	}
	else if(body->GetPosition().y < 345.f && second_trigger == true)
	{
		body->SetLinearVelocity(b2Vec2(0, 50));
	}
	else
		body->SetLinearVelocity(b2Vec2(0, 0));

}

void Elevator::activate()
{
	if (!first_trigger)
		first_trigger = true;
}

void Elevator::elevate()
{
	if (!second_trigger && first_trigger)
		second_trigger = true;
}

void Elevator::init(shared_ptr<b2World> physics_world)
{
	create_static_body(physics_world, 440, -20);
	get_body()->SetType(b2_kinematicBody);
	add_box_fixture(65, 15, b2Vec2(0, 0), 0.0f, 5.00f, 0.f, 0.5f);
	first_trigger = false;
	second_trigger = false;
}

Elevator::~Elevator()
{
}