/**
* Author: Jiale Cheng
*/

#ifndef BALL_HEADER
#define BALL_HEADER
#include "Entity.hpp"

namespace entities
{
	/**
	* @class Ball
	* @brief Entity with a round shape
	*/
	class Ball :public Entity
	{
	public:
		/**
		* @brief initialize Ball with the given values
		*/
		void init(shared_ptr<b2World> world, b2Vec2 pos, float radius, b2Vec2 relative_pos, float density, float restitution, float friction);
		Ball();
		~Ball();
		void render(RenderWindow & window);
	};
}

#endif 
