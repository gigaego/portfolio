/**
* Author: Jiale Cheng
*/

#ifndef CAR_HEADER
#define CAR_HEADER
#include "Entity.hpp"
#include "Ball.hpp"

namespace entities
{
	/**
	* @class Car
	* @brief Entity that is a vehicle with a basket
	*/
	class Car :public Entity
	{
	public:
		void init(shared_ptr<b2World> physics_world);
		Car();
		~Car();

		b2RevoluteJoint * get_basket_joint() { return car_basket_joint; }
		b2RevoluteJoint * get_wheel_joint() { return car_wheel_joint; }

		void right_acccel();
		void left_accel();
		void stop();
		void basket_active();
		void reset();
		void update() override;

		void render(RenderWindow & window);

	private:
		Entity left_wheel;
		Entity right_wheel;
		Entity car_basket;
		//Car wheels joint
		b2RevoluteJoint *car_wheel_joint;
		b2RevoluteJoint *car_wheel_joint_2;
		//joint that controls the basket
		b2RevoluteJoint *car_basket_joint;
	};

}
#endif