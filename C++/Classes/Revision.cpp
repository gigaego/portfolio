#include<iostream>
#include<string.h>
using namespace std;

class formas{
   private:
      char nombre[20];
      int lados;
   public:
      formas(){
         strcpy(nombre, " ");
         lados=0;
      }
      void setnombre(char valor[20]){
         strcpy(nombre, valor);
      }
      void setlados(int numlados){
         lados=numlados;
      }
      void getnombre(char valor[20])const{
         strcpy(valor, nombre);
      }
      int getlados()const{
         return lados;
      }
};
class rectangulo : public formas{
   protected:
      int longitud;
      int ancho;
      static int contador;
   public:
      ~rectangulo(){
      }
      rectangulo(){
         longitud=1;
         ancho=1;
         contador++;
      }
      rectangulo(int lon, int anch){
         longitud=lon;
         ancho=anch;
         contador++;
      }
      rectangulo(const rectangulo &p):longitud(p.longitud), ancho(p.ancho){
         contador++;
      }
      int area(){
         int area;
         area=longitud*ancho;
         return area;
      }
      void mostrar(){
         cout<<"longitud: "<<longitud<<endl;
         cout<<"ancho: "<<ancho<<endl;
      }
      bool operator==(rectangulo n1);
      friend int perimetro(rectangulo n1);
};

int main(){ 
   formas n1;
   char aux[20];
   n1.setnombre("cuadrado");
   n1.setlados(4);
   n1.getnombre(aux);
   cout<<aux<<endl<<n1.getlados()<<endl;
   
   rectangulo n2(2, 4);
   rectangulo n3(n2);
   rectangulo n4(3, 4);
   n3.mostrar();
   cout<<"area: "<<n3.area()<<endl;
   if(n2==n4){
      cout<<"son iguales"<<endl;
   }
   else{
      cout<<"no son iguales"<<endl;
   }
   cout<<"perimetro: "<<perimetro(n2)<<endl;
   cout<<n4.contador;
system("PAUSE");
}

int rectangulo::contador=0;
bool rectangulo :: operator==(rectangulo n1){
   if(longitud==n1.longitud && ancho==n1.ancho){
      return true;
   }
   else{
      return false;
   }
}
int perimetro(rectangulo n1){
   return (n1.longitud*2+n1.ancho*2);
}
