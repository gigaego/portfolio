#include<iostream>
using namespace std;

class color{
   private:
      float rojo;
      float verde;
      float azul;
      static int contador;
   public:
      ~color(){}
      color(float R, float V, float A){
         rojo=R;
         verde=V;
         azul=A;
         contador++;         
      }
      color(){
         rojo=0;
         verde=0;
         azul=0;
         contador++;
      }
      color(const color &p): rojo(p.rojo), verde(p.verde), azul(p.azul){
         contador++;
      }
      void setrojo(float red){
         rojo=red;
         if (rojo<0){
            rojo=0;
         }
         if (rojo>1){
            rojo=1;
         }
      }
      void setverde(float green){
         verde=green;
         if (verde<0){
            verde=0;
         }
         if (verde>1){
            verde=1;
         }
      }
      void setazul(float blue){
         azul=blue;
         if (azul<0){
            azul=0;
         }
         if (azul>1){
            azul=1;
         }
      }
      float getrojo()const{
         return rojo;
      }
      float getverde()const{
         return verde;
      }
      float getazul()const{
         return azul;
      }
      int getcontador()const{
         return contador;
      }
      void gris(){
         float media;
         media=(rojo+verde+azul)/3;
         rojo=media;
         verde=media;
         azul=media;
      }
      void filtro(float &cr, float &cg, float &cb){
         rojo=cr*rojo;
         verde=cg*verde;
         azul=cb*azul;
      }
      void leercanales(float &red, float& green, float &blue)const{
         red=rojo;
         green=verde;
         blue=azul;
      }
      friend void colorRGB(color n1, float &red, float &green, float &blue);
      color operator+(color aux);
};
int color::contador=0;
color color :: operator+(const color aux){
   color aux2;
   aux2.rojo=rojo + aux.rojo;
   if(aux2.rojo>1){
      aux2.rojo=1;
   }
   aux2.verde=verde+aux.verde;
   if(aux2.verde>1){
      aux2.verde=1;
   }
   aux2.azul=azul+aux.azul;
   if(aux2.azul>1){
      aux2.azul=1;
   }
   return aux2;
}
void colorRGB(color n1,float &red, float &green, float &blue){
   red=n1.rojo;
   green=n1.verde;
   blue=n1.azul;
}

class colorT : public color{
   private:
      float alfa;
   public:
      /*colorT(){
         alfa=0;
      }*/
      colorT(float R, float G, float B, float alpha): color(R, G, B), alfa(alpha){
         cout<<"Constructor de ColorT"<<endl;
      }
      void setalfa(float trans){
         if(trans<0){
            trans=0;
         }
         if(trans>1){
            trans=1;
         }
         alfa=trans;
      }
      float getalfa()const{
         return alfa;
      }
      void leercanales(float &red, float &green, float &blue, float &alpha){
         red=getrojo();
         green=getverde();
         blue=getazul();
         alpha=alfa;
      }
};

int main(){
   //colorT t(); Constructor que se quita porque interfiere con el nuevo
   color c(0,0,0);
   float r, v, a, alfa, r1, v1, a1;
   float cr, cg, cb;
   int n;
   cout<<"rojo: ";
   cin>>r;
   cin.ignore();
   c.setrojo(r);
   cout<<"verde: ";
   cin>>v;
   cin.ignore();
   c.setverde(v);
   cout<<"azul: ";
   cin>>a;
   cin.ignore();
   c.setazul(a);
   color copia(c);
   copia=copia+c;
   cout<<"rojo: "<<copia.getrojo()<<" verde: "<<copia.getverde()<<" azul: "<<copia.getazul()<<" contador: "<<copia.getcontador()<<endl;
   //class colorT
   cout<<"rojo: ";
   cin>>r;
   cin.ignore();
   //t.setrojo(r);
   cout<<"verde: ";
   cin>>v;
   cin.ignore();
   //t.setverde(v);
   cout<<"azul: ";
   cin>>a;
   cin.ignore();
   //t.setazul(a);
   cout<<"alfa:";
   cin>>alfa;
   cin.ignore();
   //t.setalfa(alfa);
   colorT t(r,v,a,alfa);//llamamos al constructor
   t.leercanales(r1, v1, a1, alfa);
   //cout<<"rojo: "<<t.getrojo()<<" verde: "<<t.getverde()<<" azul: "<<t.getazul()<< " alfa: "<<t.getalfa()<<" contador: "<<t.getcontador()<<endl;
   cout<<"rojo: "<<r1<<" verde: "<<v1<<" azul: "<<a1<< " alfa: "<<alfa<<endl;
   t.color::leercanales(r, v, a);
   cout<<"rojo: "<<r<<" verde: "<<v<<" azul: "<<a<<endl;
   /*PRUEBAS
   c.gris();
   cout<<endl<<"GRIS"<<endl;
   cout<<"rojo: "<<c.getrojo()<<" verde: "<<c.getverde()<<" azul: "<<c.getazul()<<endl;
   */
   /*
   cout<<"FILTRO: "<<endl<<"1.ROJO"<<endl<<"2.VERDE"<<endl<<"3.AZUL"<<endl;//FILTRO
   cin>>n;
   cin.ignore();
   if(n==1){//filtro rojo
      cr=1;
      cg=0;
      cb=0;
   }
   if(n==2){//filtro verde
      cr=0;
      cg=1;
      cb=0;
   }
   if(n==3){//filtro azul
      cr=0;
      cg=0;
      cb=1;
   }
   c.filtro(cr, cg, cb);
   cout<<"rojo: "<<c.getrojo()<<" verde: "<<c.getverde()<<" azul: "<<c.getazul()<<endl;
   */
   colorRGB(c, r, v, a);
   cout<<"rojo: "<<r<<" verde: "<<v<<" azul: "<<a<<endl;
system("PAUSE");
}

