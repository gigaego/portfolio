#include <iostream>
#include <string.h>
using namespace std;

class videojuego {
   private:
      char titulo[20];
      char compania[20];
      char genero[20];
      int pegi, stock;
      float preciocompra, pvp;
   public:
      
      //constructor
      videojuego(){
         strcpy(titulo, " ");
         strcpy(compania, " ");
         strcpy(genero, " ");
         pegi=0;
         preciocompra=0;
         pvp=0;
         stock=0;
      }
      void guarda(char title[20], char company[20], char gen[20], int edad, float precioc, float preciov, int unidades){
         strcpy(titulo, title);
         strcpy(compania, company);
         strcpy(genero, gen);
         pegi=edad;
         preciocompra=precioc;
         pvp=preciov;
         stock=unidades;
      }
      void lee(char title[20], char company[20], char gen[20], int &edad, float &precioc, float &preciov, int &unidades){
         strcpy(title, titulo);
         strcpy(company, compania);
         strcpy(gen, genero);
         edad=pegi;
         precioc=preciocompra;
         preciov=pvp;
         unidades=stock;
      }
};


int main(){
   videojuego n1;
   videojuego n2;
   char titulo1[20];
   char compania1[20];
   char genero1[20];
   int pegi1;
   float preciocompra1, pvp1;
   int stock1;
   
   n1.guarda("jiale game", "my games", "lucha", 18, 20, 40, 100);
   n2.lee(titulo1, compania1, genero1, pegi1, preciocompra1, pvp1, stock1);
   cout<<titulo1<<endl<<compania1<<endl<<genero1<<endl<<pegi1<<endl<<preciocompra1<<endl<<pvp1<<endl<<stock1<<endl;

system("PAUSE");
}
