#ifndef INPUTS_MESSAGE_HEADER
#define INPUTS_MESSAGE_HEADER
#include <Message_handler.hpp>
#include <Task.hpp>
#include <Windows.h>

using namespace engine;

namespace pr01
{
	struct Inputs_Message : public Message
	{
		class Inputs_Task : public Task
		{
			Inputs_Message  * message;
			Message_Handler * handler;
		public:
			Inputs_Task(Inputs_Message * message, Message_Handler * handler)
				: message(message), handler(handler)
			{}
			~Inputs_Task()
			{}

			bool initialize()
			{
				return true;
			}

			bool do_step(float time)
			{
				check_inputs();
				handler->multicast(*message);
				return true;
			}

			bool finalize()
			{
				return true;
			}

			void check_inputs()
			{
				if (GetAsyncKeyState(VK_UP))
				{
					message->up = true;
					message->add_parameter("up", message->up);
				}
				else if (!GetAsyncKeyState(VK_UP))
				{
					message->up = false;
					message->add_parameter("up", message->up);
				}

				if (GetAsyncKeyState(VK_DOWN))
				{
					message->down = true;
					message->add_parameter("down", message->down);
				}

				else if (!GetAsyncKeyState(VK_DOWN))
				{
					message->down = false;
					message->add_parameter("down", message->down);
				}

				if (GetAsyncKeyState(VK_RIGHT))
				{
					message->right = true;
					message->add_parameter("right", message->right);
				}

				else if (!GetAsyncKeyState(VK_RIGHT))
				{
					message->right = false;
					message->add_parameter("right", message->right);
				}

				if (GetAsyncKeyState(VK_LEFT))
				{
					message->left = true;
					message->add_parameter("left", message->left);
				}

				else if (!GetAsyncKeyState(VK_LEFT))
				{
					message->left = false;
					message->add_parameter("left", message->left);
				}
			}
		private:

		};
		Inputs_Task task;
		Variant up;
		Variant down;
		Variant left;
		Variant right;

		Inputs_Message(Message_Handler * handler):Message((string)"inputs"), task(this, handler)
		{
			add_parameter("up", up);
			add_parameter("down", down);
			add_parameter("left", left);
			add_parameter("right", right);
		}
	};
}
#endif // !INPUTS_MESSAGE_HEADER
