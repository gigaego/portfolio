//#ifndef PLAYER_HEADER
//#define PLAYER_HEADER

#include <Entity.hpp>
#include <Physics_Module.hpp>
#include <Render_Module.hpp>
#include <Model.hpp>
#include <Cube.hpp>

using namespace engine;

namespace pr01
{
	class Player : public Entity
	{
	private:
		class Update_Graphics_Task :public Task
		{
			Player * player;

			shared_ptr< Physics_Module::Physics_Component > physics_comp;

			shared_ptr< Render_Module::Render_Component   > rend_comp;
		public:
			Update_Graphics_Task(Player * player) :player(player)
			{
			}
			~Update_Graphics_Task() {}

			bool initialize()
			{
				rend_comp = dynamic_pointer_cast<Render_Module::Render_Component> (player->get_component((string)"render"));
				physics_comp = dynamic_pointer_cast<Physics_Module::Physics_Component> (player->get_component((string)"physics"));

				return true;
			}

			bool do_step(float time)
			{
				// Update the transform component to match the physics transform
				btTransform transformation;
				physics_comp->get_body()->getMotionState()->getWorldTransform(transformation);
				float matrix[16];
				transformation.getOpenGLMatrix(matrix);
				player->body->update_btTransform(matrix);
				player->scale();
				return true;
			}

			bool finalize()
			{
				return true;
			}

		};

	private:

		Update_Graphics_Task task;
		float scale_x, scale_y, scale_z;

	public:
		shared_ptr< Model > body;
		Player(
			float scale_x, float scale_y, float scale_z,
			float pos_x, float pos_y, float pos_z,
			bool box_shape, bool static_body, bool rotation,
			shared_ptr<Physics_Module> physics,
			shared_ptr<Render_Module> render)
			: Entity(), scale_x(scale_x), scale_y(scale_y), scale_z(scale_z), task(this)
		{

			// Create the physics component using the physics module

			physics->create_component(this);

			shared_ptr< Physics_Module::Physics_Component > physics_comp
				= dynamic_pointer_cast<Physics_Module::Physics_Component> (get_component((string)"physics"));

			physics_comp->position = toolkit::Vector3(pos_x, pos_y, pos_z);
			physics_comp->scale = toolkit::Vector3(scale_x, scale_y, scale_z);
			physics_comp->box_shape = box_shape;
			physics_comp->static_body = static_body;
			physics_comp->rotation = rotation;

			render->create_component(this);

			physics_comp->get_body();

			// Create the render component and add it to the render module

			shared_ptr< Render_Module::Render_Component > rend_comp
				= dynamic_pointer_cast<Render_Module::Render_Component> (get_component((string)"render"));

			body.reset(new Model);

			body->scale(scale_x, scale_y, scale_z);
			body->translate(toolkit::Vector3(pos_x, pos_y, pos_z));
			rend_comp->item.material = Material::default_material();
			rend_comp->item.mesh.reset(new Cube());

			body->add(rend_comp->item.mesh, rend_comp->item.material);

			body->set_visible(true);

			physics_comp->initialize();

			render->add("player", body);

		}

		~Player()
		{}

	public:

		void scale()
		{
			transform.local_scale = toolkit::scale(transform.local_scale, scale_x, scale_y, scale_z);
		}

		Task * get_task()
		{
			return &task;
		}

	};
}
//#endif // !PLAYER_HEADER
