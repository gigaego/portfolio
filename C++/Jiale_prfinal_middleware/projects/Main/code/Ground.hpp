#ifndef GROUND_HEADER
#define GROUND_HEADER

#include <Entity.hpp>
#include <Physics_Module.hpp>
#include <Render_Module.hpp>
#include <Model.hpp>
#include <Cube.hpp>
using namespace engine;
namespace pr01
{
	class Ground :public Entity
	{
	public:
		Ground(
			float scale_x, float scale_y, float scale_z,
			float pos_x, float pos_y, float pos_z,
			shared_ptr<Physics_Module> physics,
			shared_ptr<Render_Module> render)
			: Entity()
		{
			// Create the physics component using the physics module

			physics->create_component(this);

			shared_ptr< Physics_Module::Physics_Component > physics_comp
				= dynamic_pointer_cast<Physics_Module::Physics_Component> (get_component((string)"physics"));

			physics_comp->position	  = toolkit::Vector3(pos_x, pos_y, pos_z);
			physics_comp->scale		  = toolkit::Vector3(scale_x, scale_y, scale_z);
			physics_comp->box_shape	  = true;
			physics_comp->static_body = true;
			physics_comp->rotation    = false;

			render->create_component(this);

			physics_comp->get_body();

			// Create the render component and add it to the render module

			shared_ptr< Render_Module::Render_Component > rend_comp
				= dynamic_pointer_cast<Render_Module::Render_Component> (get_component((string)"render"));

			shared_ptr< Model >body(new Model);

			rend_comp->item.material = Material::default_material();
			rend_comp->item.mesh.reset(new Cube());

			body->add(rend_comp->item.mesh, rend_comp->item.material);

			body->set_visible(true);

			physics_comp->initialize();

			btTransform transformation;
			physics_comp->get_body()->getMotionState()->getWorldTransform(transformation);
			float matrix[16];
			transformation.getOpenGLMatrix(matrix);
			body->update_btTransform(matrix);
			body->scale(scale_x, scale_y, scale_z);

			render->add("ground", body);
		}
		~Ground()
		{
		}

	};
}

#endif // !GROUND_HEADER

