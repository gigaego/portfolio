#ifndef INPUT_LISTENER_HEADER
#define INPUT_LISTENER_HEADER

#include <Message_handler.hpp>
#include <iostream>
#include "Player.hpp"
using namespace engine;

namespace pr01
{
	struct Input_Listener : Message_Handler::Listener
	{
		shared_ptr< Physics_Module::Physics_Component > player_phy_comp;
		float speed;

		Input_Listener(engine::Message_Handler * supervisor, Player * player, float speed)
			: speed(speed)
		{
			supervisor->register_listener((string)"inputs", *this);
			player_phy_comp = dynamic_pointer_cast<Physics_Module::Physics_Component> (player->get_component((string)"physics"));
		}
		void handle_message(Message & keypressed) override
		{
			const string key_up	   = "up";
			const string key_down  = "down";
			const string key_left  = "left";
			const string key_right = "right";

			if (keypressed.id == "inputs")
			{
				// up
				if (keypressed.parameters[key_up].as_bool() == true
					&& keypressed.parameters[key_down].as_bool() == false)
				{
					std::cout << " up true" << std::endl;
					player_phy_comp->set_z_linear_velocity(-speed);
				}

				else if (keypressed.parameters[key_up].as_bool() == false
					&& keypressed.parameters[key_down].as_bool() == false)
				{
					player_phy_comp->set_z_linear_velocity(0);
				}

				// down
				else if (keypressed.parameters[key_down].as_bool() == true
					&& keypressed.parameters[key_up].as_bool() == false)
				{
					std::cout << " down true" << std::endl;
					player_phy_comp->set_z_linear_velocity(speed);
				}

				// left
				if (keypressed.parameters[key_left].as_bool() == true
					&& keypressed.parameters[key_right].as_bool() == false)
				{
					std::cout << " left true" << std::endl;
					player_phy_comp->set_x_linear_velocity(-speed);
				}

				else if (keypressed.parameters[key_left].as_bool() == false
					&& keypressed.parameters[key_right].as_bool() == false)
				{
					player_phy_comp->set_x_linear_velocity(0);
				}

				// right
				else if (keypressed.parameters[key_right].as_bool() == true
					&& keypressed.parameters[key_left].as_bool() == false)
				{
					std::cout << " right true" << std::endl;
					player_phy_comp->set_x_linear_velocity(speed);
				}

			}

		}

	};
}
#endif // !INPUT_LISTENER_HEADER


