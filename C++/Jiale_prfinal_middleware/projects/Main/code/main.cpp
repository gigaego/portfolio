/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Started by �ngel on april of 2016                                          *
*                                                                             *
*  This is free software released into the public domain.                     *
*                                                                             *
*  angel.rodriguez@esne.edu                                                   *
*																			  *
*  Modified by Jiale Cheng													  *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <iostream>
#include <My_Scene.hpp>
#include <Render_Module.hpp>
#include <Physics_Module.hpp>
#include <glbinding/gl/gl.h>
#include <glbinding/Binding.h>
#include <SFML/Window.hpp>
#include "Ground.hpp"
#include "Input_Listener.hpp"
#include "Inputs_Message.hpp"

using namespace std;
using namespace gl;
using namespace engine;
int main ()
{
	sf::Window window
	(
		sf::VideoMode(800, 600),
		"Jiale pr Middleware",
		sf::Style::Default,
		sf::ContextSettings(24, 0, 0, 3, 2, sf::ContextSettings::Core)      // Se usa OpenGL 3.2 core profile
	);

	// Se determinan las caracter�sticas de OpenGL disponibles en la m�quina:

	glbinding::Binding::initialize();

	// Se activa la sincronizaci�n vertical:

	window.setVerticalSyncEnabled(true);

	My_Scene scene;
	shared_ptr< Render_Module > render(new Render_Module(&scene, &window));
	scene.add_module("render", render);

	shared_ptr< Physics_Module > physics(new Physics_Module(&scene));
	scene.add_module("physics", physics);

	pr01::Player player(1.f, 1.f, 1.f, 0, -10, -50, false, false, false, physics, render);
	pr01::Ground ground(20.f, 3.f, 20.f, 0.f, -14.1, -50, physics, render);

	Message_Handler      inputs;
	pr01::Input_Listener input_listener(&inputs, &player, 10.f);
	pr01::Inputs_Message inputs_message(&inputs);

	scene.add_task(player.get_task());

	scene.add_task(&inputs_message.task);

	scene.init_kernel();
	
	glClearColor(0.1f, 0.1f, 0.1f, 1.f);

	// Bucle principal:
	scene.update();

	return (EXIT_SUCCESS);

	cin.get();

    return 0;
}