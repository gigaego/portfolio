#ifndef TRANSFORM_MODULE_HEADER
#define TRANSFORM_MODULE_HEADER

#include "Module.hpp"

namespace engine
{
	class Transform_Module : public Module
	{
	public:
		~Transform_Module() {}
		class Transform_task : public Task
		{
			Transform_Module & module;
		public:

			Transform_task(Transform_Module & module) : module(module)
			{
			}

			bool initialize()
			{
				// ...
				return true;
			}

			bool finalize()
			{
				// ...
				return true;
			}

			bool do_step(float time)
			{
				// ...
				return true;
			}


		};

		class Transform_Component : public Component
		{
		public:

			public:

			toolkit::Matrix44 local_scale;
			toolkit::Matrix44 local_anchor;
			toolkit::Matrix44 transformation;

			Transform_Component(Entity * entity) : Component(entity)
			{
			}

			bool initialize()
			{
				transformation = local_scale * local_anchor;
				return true;
			}

			bool parse_property
			(
				const string & name,
				const string & value
			)
			{
				return true;
			}

		};

		Transform_task task;
		My_Scene * scene;

	public:

		Transform_Module(My_Scene * scene) : task(*this), Module(scene)
		{
		}

		Task * get_task() override
		{
			return &task;
		}

	};

}
#endif // !TRANSFORM_MODULE_HEADER

