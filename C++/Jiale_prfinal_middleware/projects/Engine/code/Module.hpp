#ifndef MODULE_HEADER
#define MODULE_HEADER

#include "Task.hpp"
#include "Component.hpp"
#include <memory>
#include <btBulletDynamicsCommon.h>
#include <Math.hpp>

namespace engine
{
	class My_Scene;
	class Module
	{
	protected:
		My_Scene * owner;

	public:

		// Reimplementar si se tiene Task.
		virtual Task * get_task()
		{
			return nullptr;
		}

	public:

		Module(My_Scene * owner) : owner(owner)
		{}

		virtual ~Module()
		{
		}

		virtual void create_component(Entity * entity)
		{
		}
	};
}
#endif // !MODULE_HEADER

