#ifndef KERNEL_HEADER
#define KERNEL_HEADER

#include <set>
#include "Task.hpp"

namespace engine
{
	class Kernel
	{
		typedef std::multiset< Task * > Task_List;

		Task_List tasks;

		bool exit;
		//bool paused;

	public:

		Kernel() 
		{
		}

		void add_task(Task * task)
		{
			tasks.insert(task);
			task->set_kernel(this);
		}

		bool execute()
		{
			exit = false;

			for (auto task : tasks)
			{
				task->initialize();
			}

			float time = 0.017f;

			while (!exit)
			{
				for (auto task : tasks)
				{
					if (task->do_step(time) == true)
					{
						exit = false;
					}
					else
					{
						exit = true;
						break;
					}

				}
			}

			for (auto task : tasks)
			{
				task->finalize();
			}
			return false;

		}

		void stop()
		{
			exit = true;
		}

		/*void pause()
		{
			paused = true;
		}

		void resume()
		{
			paused = false;
		}*/

	};
}

#endif // !KERNEL_HEADER

