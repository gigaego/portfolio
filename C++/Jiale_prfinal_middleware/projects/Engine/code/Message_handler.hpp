#ifndef MESSAGE_HANDLER
#define MESSAGE_HANDLER

#include <map>
#include <list>
#include "Message.hpp"

namespace engine
{
	class Message_Handler
	{
	public:

		struct Listener
		{
			virtual void handle_message(Message & message) = 0;
		};

	private:
		
		std::map< std::string, std::list< Listener * > > listeners;

	public:

		void register_listener(std::string & message_id, Listener & listener)
		{
			listeners[message_id].push_back(&listener);
		}

		void unregister_listener(std::string & message_id, Listener & listener)
		{
			// ...
		}

		// Enviar a todos los listeners.
		void multicast(Message & message)
		{
			// Comprobar si alg�n listener puede recibir el mensaje
			if (listeners.count(message.id) > 0)
			{
				// Cada listener se encargar�a de manejar el mensaje
				for (auto listener : listeners[message.id])
				{
					listener->handle_message(message);
				}
			}
		}


	};

}
#endif // !MESSAGE_HANDLER
