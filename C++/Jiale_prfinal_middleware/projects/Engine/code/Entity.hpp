#ifndef ENTITY_HEADER
#define ENTITY_HEADER

#include <map>
#include "Transform_Module.hpp"

namespace engine
{
	class Entity
	{
	protected:
		Transform_Module::Transform_Component transform;

		map< string, shared_ptr< Component > > components;

	public:
		Entity() : transform(this)
		{
		}

		bool initialize()
		{
			bool result = true;

			for (auto component : components)
			{
				if (component.second->initialize() == false)
				{
					result = false;
				}
			}

			if (transform.initialize() == false)
			{
				result = false;
			}

			return result;
		}

		void scale(float x, float y, float z)
		{
			transform.local_scale = toolkit::scale(transform.local_scale, x, y, z);
		}

		Transform_Module::Transform_Component * get_transform()
		{
			return &transform;
		}

		bool add_component
		(
			const string & type,
			shared_ptr< Component > component
		)
		{
			// The component already exists
			if (components.count(type) != 0)
			{
				return false;
			}
			else
			{
				components[type] = component;
				return true;
			}
		}

		shared_ptr< Component > get_component(string & type)
		{
			if (components.count(type) != 0)
			{
				return components[type];
			}
			else
			{
				return nullptr;
			}
		}

	};

}

#endif // !ENTITY_HEADER
