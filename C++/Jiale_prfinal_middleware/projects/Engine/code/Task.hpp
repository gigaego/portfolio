/*
	Author: Jiale Cheng
*/
#ifndef TASK_HEADER
#define TASK_HEADER

namespace engine
{
	class Kernel;
	class Task
	{
	protected:
		Kernel * kernel;

		int priority;

	public:
		Task() {}

		Task(int priority) :priority(priority)
		{
			kernel = nullptr;
		}

		~Task() {}

		void set_kernel(Kernel * new_kernel)
		{
			kernel = new_kernel;
		}

	public:

		virtual bool initialize() = 0;
		virtual bool finalize() = 0;
		virtual bool do_step(float time) = 0;

	public:

		bool operator < (const Task & other) const
		{
			return this->priority < other.priority;
		}

		bool operator > (const Task & other) const
		{
			return this->priority > other.priority;
		}

	};

}
#endif // !TASK_HEADER
