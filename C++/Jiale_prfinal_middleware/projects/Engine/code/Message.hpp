#ifndef MESSAGE_HEADER
#define MESSAGE_HEADER

#include <string>
#include <map>
#include "Variant.hpp"

namespace engine
{
	struct Message
	{

		std::string id;

		std::map< std::string, Variant > parameters;

		Message(const std::string & id) : id(id)
		{
		}

		void add_parameter(std::string name, Variant & value)
		{
			parameters[name] = value;
		}

		Variant get_variant(std::string name)
		{
			return parameters[name];
		}

	};
}
#endif // !MESSAGE_HEADER
