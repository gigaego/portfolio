#ifndef COMPONENT_HEADER
#define COMPONENT_HEADER

#include <string>

using namespace std;
namespace engine
{
	class Entity;
	class Component
	{
	protected:

		Entity * parent;

	public:

		Component() {}

		Component(Entity * parent) : parent(parent)
		{
		}

		virtual ~Component()
		{
		}

		virtual bool initialize() = 0;

		virtual bool parse_property
		(
			const string & name,
			const string & value
		) = 0;

		Entity * get_parent()
		{
			return parent;
		}

	};
}

#endif // !COMPONENT_HEADER
