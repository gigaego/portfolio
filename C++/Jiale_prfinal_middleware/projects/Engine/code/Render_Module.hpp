#ifndef RENDER_MODULE_HEADER
#define RENDER_MODULE_HEADER

#include "Module.hpp"
#include <Material.hpp>
#include <Camera.hpp>
#include <Light.hpp>
#include <Drawable.hpp>
#include <Model.hpp>
#include "Entity.hpp"
#include <SFML/Window.hpp>


using namespace toolkit;
namespace engine
{
	class Render_Module : public Module
	{
	private:
		typedef list< Drawable *								   > Drawables;
		typedef map < Scene_Object *,   Drawables			   	   > Drawable_By_Objects;
		typedef map < Material *,	    Drawable_By_Objects		   > Drawable_By_Material;
		typedef map < Shader_Program *, Drawable_By_Material	   > Drawables_By_Shader;
		typedef map < string,			shared_ptr< Scene_Object > > Scene_Object_Map;
		typedef list< Scene_Object *							   > Scene_Object_List;

	private:

		static const size_t max_number_of_lights = 3;

	public:

		bool add(const std::string & name, std::shared_ptr< Scene_Object > scene_object)
		{
			// No puede haber varios scene objects con el mismo nombre:

			if (scene_objects.find(name) != scene_objects.end())
			{
				assert(false);                      // Cuando est� activa la configuraci�n debug se resalta el error
				return false;
			}

			scene_objects[name] = scene_object;

			// Si es un modelo 3D, se a�aden sus drawables a la lista de drawables estructurada para optimizar el render:

			Model * model = dynamic_cast< Model * >(scene_object.get());

			if (model != nullptr)
			{
				for (const Model::Piece & piece : model->get_pieces())
				{
					drawable_list[piece.material->get_shader_program()][piece.material.get()][scene_object.get()].push_back(piece.drawable.get());
				}
			}

			// Si es una c�mara, se establece como c�mara por defecto si no hay alguna:

			if (active_camera == nullptr)
			{
				active_camera = dynamic_cast< Camera * >(scene_object.get());         // Se queda nulo si no es una c�mara
			}

			// Si es una luz, se le asigna un �ndice en la escena:

			Light * light = dynamic_cast< Light * >(scene_object.get());

			if (light != nullptr)
			{
				light->set_light_index(number_of_lights < max_number_of_lights ? number_of_lights : -1);

				number_of_lights++;
			}

			// Si el scene object modifica alguna variable uniforme de los shaders, se le a�ade a una lista:

			if (scene_object->changes_shaders())
			{
				shader_update_list.push_back(scene_object.get());
			}

			return true;
		}

		bool render()
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			sf::Event event;

			while (window->pollEvent(event))
			{
				switch (event.type)
				{
				case sf::Event::Closed:
				{
					return false;
					break;
				}

				case sf::Event::Resized:
				{
					reset_viewport(*window);
					break;
				}
				}
			}
			if (active_camera == nullptr)
			{
				assert(false);                  // Se resalta el error cuando est� activa la configuraci�n debug
				return false;
			}

			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);

			const Matrix44 view_matrix = active_camera->get_inverse_transformation();

			// Para cada shader se recorren los materiales que lo usan:

			for (auto & drawables_by_shader : drawable_list)
			{
				// Se pone en uso el shader_program que usan cierto grupo de materiales y drawables:

				Shader_Program * active_shader_program = drawables_by_shader.first;

				active_shader_program->use();

				// Se ajusta alguna variable uniforme general:

				active_shader_program->set_uniform_value(active_shader_program->get_uniform_id("projection_matrix"), active_camera->get_projection_matrix());

				// Se avisa a los scene_objects que necesitan ajustar alguna variable uniforme:

				for (auto & scene_object : shader_update_list)
				{
					scene_object->shader_changed(*active_shader_program);
				}

				// Para cada material se recorren los drawables que lo usan:

				for (auto & drawables_by_material : drawables_by_shader.second)
				{
					// Se establece el material activo (puede modificar algunas variables uniformes):

					Material * material = drawables_by_material.first;

					material->use();

					// Para cada grupo de drawables que usan cierto material se recorren los drawables de un mismo Scene_Object:

					for (auto & drawables_by_object : drawables_by_material.second)
					{
						if (drawables_by_object.first->is_visible())
						{
							// Se calculan las matrices de transformaci�n del objeto y se actualizan las variables uniformes correspondientes:

							const Matrix44 model_view_matrix = view_matrix * drawables_by_object.first->get_transformation();
							const Matrix44 model_view_normal_matrix = transpose(inverse(model_view_matrix));

							active_shader_program->set_uniform_value(active_shader_program->get_uniform_id("model_view_matrix"), model_view_matrix);
							active_shader_program->set_uniform_value(active_shader_program->get_uniform_id("model_view_normal_matrix"), model_view_normal_matrix);

							// Se dibujan los drawables de un mismo Scene_Object que comparten cierto material:

							for (auto & drawable : drawables_by_object.second)
							{
								drawable->draw();
							}
						}
					}
				}
			}
			window->display();
			return true;
		}

		~Render_Module() {}

		class Render_task : public Task
		{
		private:
			Render_Module & module;
		public:

			Render_task(Render_Module & module) : module(module), Task(1)
			{
			}

			bool initialize()
			{
				// ...
				return true;
			}

			bool finalize()
			{
				// ...
				return true;
			}

			bool do_step(float time)
			{
				if (module.render() == true)
				{
					return true;
				}
				return false;
			}


		};

		class Render_Component : public Component
		{
		public:

			class Piece
			{
			public:
				shared_ptr < Drawable > mesh;
				shared_ptr < Material > material;
			};

			Piece item;

			Render_Component(Entity * entity) : Component(entity)
			{
				
			}

			bool initialize()
			{
				return true;
			}

			bool parse_property
			(
				const string & name,
				const string & value
			)
			{
				return true;
			}
		};

		Render_task			task;

		Scene_Object_Map    scene_objects;                      ///< Mapa de todos los objetos de la escena.
		Drawables_By_Shader drawable_list;                      ///< Lista estructurada de drawables que hay que renderizar.
		Scene_Object_List   shader_update_list;                 ///< Lista de scene objects que hay que avisar cuando se cambia de shader.

		Camera            * active_camera;                      ///< Puntero a la c�mara activa. Debe haber una para poder hacer un render.
		GLuint              number_of_lights;                   ///< Contador de luces presentes en la escena. Se utiliza para asignar un �ndice a cada una.

		sf::Window		  * window;

	public:

		Render_Module(My_Scene * scene, sf::Window * window) : task(*this), Module (scene), window(window)
		{
			active_camera	 = nullptr;
			number_of_lights = 0;

			shared_ptr< Camera > camera(new Camera(20.f, 1.0f, 100.f, 1.f));
			shared_ptr< Light  > light(new Light);

			add("camera", camera);
			add("light", light);

			light->translate(Vector3(0.f, 10.f, -10.f));
		}

		Task * get_task() // Override
		{
			return &task;
		}

		void create_component(Entity * entity) override
		{
			shared_ptr< Render_Component > rend_comp(new Render_Component(entity));
			entity->add_component("render", rend_comp);
		}

		void reset_viewport(const sf::Window & window)
		{
			GLsizei  width = GLsizei(window.getSize().x);
			GLsizei  height = GLsizei(window.getSize().y);

			active_camera->set_aspect_ratio(float(width) / height);

			glViewport(0, 0, width, height);
		}

	};

}
#endif // !RENDER_MODULE_HEADER

