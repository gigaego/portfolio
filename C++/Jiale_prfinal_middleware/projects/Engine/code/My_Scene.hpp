#ifndef MY_SCENE_HEADER
#define MY_SCENE_HEADER

#include "Entity.hpp"
#include "Kernel.hpp"

namespace engine
{

	class My_Scene
	{

		typedef map< string, shared_ptr< Module > > Module_Map;
		typedef map< string, shared_ptr< Entity > > Entity_Map;

		Module_Map modules;
		Kernel     kernel;
		Entity_Map entities;

	public:

		My_Scene()
		{
		}

		void init_kernel()
		{
			// Por cada M�dulo
			for (auto module : modules)
			{
				// A�adimos el Task al kernel
				Task * task = module.second->get_task();

				if (task)
				{
					kernel.add_task(task);
				}
			}
		}

		void add_module(string type, shared_ptr< Module > module)
		{
			modules[type] = module;
		}

		void add_entity(string type, shared_ptr< Entity > entity)
		{
			entities[type] = entity;
		}

		void add_task(Task * task)
		{
			if(task)
				kernel.add_task(task);
		}

		bool update()
		{
			if (kernel.execute() == true)
			{
				return true;
			}
			return false;
		}

	private:


	};
}
#endif // !MY_SCENE_HEADER
