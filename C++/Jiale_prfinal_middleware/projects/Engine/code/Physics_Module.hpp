#ifndef PHYSICS_MODULE_HEADER
#define PHYSICS_MODULE_HEADER

#include "Module.hpp"
#include "Entity.hpp"

namespace engine
{
	class Physics_Module : public Module
	{
	public:
		~Physics_Module() {}

		class Physics_task : public Task
		{
		private:
			Physics_Module  & module;

		public:

			Physics_task(Physics_Module & module) : module(module), Task(0)
			{
			}

			bool initialize()
			{
				return true;
			}

			bool finalize()
			{
				return true;
			}

			bool do_step(float time)
			{
				module.world->stepSimulation(time);
				return true;
			}


		};

		class Physics_Component : public Component
		{	
		private:
			shared_ptr< btCollisionShape > shape;
			shared_ptr< btRigidBody      > body;

			Physics_Module				 * module;
			
		public:

			toolkit::Vector3 scale;
			toolkit::Vector3 position;

			btTransform * transform;

			bool box_shape;
			bool static_body;
			bool rotation;

			Physics_Component(Entity * parent, Physics_Module * module)
				: Component(parent), module(module)
			{
			}

			bool parse_property
			(
				const string & name,
				const string & value
			)
			{
				return true;
			}

			bool initialize()
			{
				if (box_shape && static_body)
				{
					init_static_box_body();
					return true;
				}

				else if (box_shape && !static_body)
				{
					init_dynamic_box_body();
					return true;
				}

				else if (!box_shape && static_body)
				{
					init_static_sphere_body();
					return true;
				}

				else if (!box_shape && !static_body)
				{
					init_dynamic_sphere_body();
					return true;
				}
				return false;
			}

			shared_ptr< btRigidBody > get_body()
			{
				return body;
			}

			void add_to_world()
			{
				module->get_world()->addRigidBody(body.get());
			}
			
			void set_x_linear_velocity(float speed)
			{
				float speed_y = body->getLinearVelocity().y();
				float speed_z = body->getLinearVelocity().z();
				body->setLinearVelocity(btVector3(speed, speed_y, speed_z));
			}

			void set_y_linear_velocity(float speed)
			{
				float speed_x = body->getLinearVelocity().x();
				float speed_z = body->getLinearVelocity().z();
				body->setLinearVelocity(btVector3(speed_x, speed, speed_z));
			}

			void set_z_linear_velocity(float speed)
			{
				float speed_x = body->getLinearVelocity().x();
				float speed_y = body->getLinearVelocity().y();
				body->setLinearVelocity(btVector3(speed_x, speed_y, speed));
			}

			void init_static_box_body()
			{
				shape.reset(new btBoxShape(btVector3(scale.x, scale.y, scale.z)));

				btTransform transform;
				transform.setIdentity();
				transform.setOrigin(btVector3(position.x, position.y, position.z));

				std::shared_ptr< btDefaultMotionState >  state(new btDefaultMotionState(transform));
				btRigidBody::btRigidBodyConstructionInfo info(0, state.get(), shape.get());

				body.reset(new btRigidBody(info));

				add_to_world();

				module->push_back(body);
				module->push_back(state);
				module->push_back(shape);
			}

			void init_dynamic_box_body()
			{
				shape.reset(new btBoxShape(btVector3(scale.x, scale.y, scale.z)));

				btTransform transform;
				transform.setIdentity();
				transform.setOrigin(btVector3(position.x, position.y, position.z));

				btScalar  mass = 5.f;
				btVector3 localInertia(0, 0, 0);

				std::shared_ptr< btDefaultMotionState >  state(new btDefaultMotionState(transform));
				btRigidBody::btRigidBodyConstructionInfo info(mass, state.get(), shape.get(), localInertia);

				body.reset(new btRigidBody(info));

				// The body won't fall asleep
				body->setActivationState(DISABLE_DEACTIVATION);

				if (!rotation)
				{
					// The body won't rotate
					body->setAngularFactor(btVector3(0, 0, 0));
				}

				add_to_world();

				module->push_back(body);
				module->push_back(state);
				module->push_back(shape);
			}

			void init_static_sphere_body()
			{
				shape.reset(new btSphereShape(scale.x));

				btTransform transform;
				transform.setIdentity();
				transform.setOrigin(btVector3(position.x, position.y, position.z));

				std::shared_ptr< btDefaultMotionState >  state(new btDefaultMotionState(transform));
				btRigidBody::btRigidBodyConstructionInfo info(0, state.get(), shape.get());

				body.reset(new btRigidBody(info));

				add_to_world();

				module->push_back(body);
				module->push_back(state);
				module->push_back(shape);

			}

			void init_dynamic_sphere_body()
			{
				shape.reset(new btSphereShape(scale.x));

				btTransform transform;
				transform.setIdentity();
				transform.setOrigin(btVector3(position.x, position.y, position.z));

				btScalar  mass = 5.f;
				btVector3 localInertia(0, 0, 0);

				std::shared_ptr< btDefaultMotionState >  state(new btDefaultMotionState(transform));
				btRigidBody::btRigidBodyConstructionInfo info(mass, state.get(), shape.get(), localInertia);

				body.reset(new btRigidBody(info));

				// The body won't fall asleep
				body->setActivationState(DISABLE_DEACTIVATION);

				if (!rotation)
				{
					// The body won't rotate
					body->setAngularFactor(btVector3(0, 0, 0));
				}

				add_to_world();

				module->push_back(body);
				module->push_back(state);
				module->push_back(shape);

			}

		};

		Physics_task task;

		btDefaultCollisionConfiguration		collisionConfiguration;
		btCollisionDispatcher			  * collisionDispatcher;
		btDbvtBroadphase					overlappingPairCache;
		btSequentialImpulseConstraintSolver constraintSolver;


		btDiscreteDynamicsWorld  * world;

		vector< shared_ptr< btRigidBody          > > rigidBodies;
		vector< shared_ptr< btDefaultMotionState > > motionStates;
		vector< shared_ptr< btCollisionShape     > > collisionShapes;

		My_Scene * scene;

	public:

		Physics_Module(My_Scene * scene) : task(*this), Module(scene)
		{
			
			collisionDispatcher = new btCollisionDispatcher(&collisionConfiguration);
			world =new btDiscreteDynamicsWorld
			(
				collisionDispatcher,
				&overlappingPairCache,
				&constraintSolver,
				&collisionConfiguration
			);
			world->setGravity(btVector3(0, -10, 0));

		}

		Task * get_task() // Override
		{
			return &task;
		}

		btDiscreteDynamicsWorld * get_world()
		{
			return world;
		}

		void create_component(Entity * entity) override
		{
			shared_ptr< Physics_Component > phys_comp(new Physics_Component(entity, this));
			entity->add_component("physics", phys_comp);
		}

		void push_back(shared_ptr< btRigidBody > body)
		{
			rigidBodies.push_back(body);
		}

		void push_back(shared_ptr< btCollisionShape > shape)
		{
			collisionShapes.push_back(shape);
		}

		void push_back(shared_ptr< btDefaultMotionState > motion)
		{
			motionStates.push_back(motion);
		}

	};

}
#endif // !PHYSICS_MODULE_HEADER
